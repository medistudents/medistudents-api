'use strict';

import plugins  from 'gulp-load-plugins';
import yargs    from 'yargs';
import browser  from 'browser-sync';
import connect  from 'gulp-connect-php';
import gulp     from 'gulp';
import yaml     from 'js-yaml';
import rimraf   from 'rimraf';
import fs       from 'fs';
import change   from 'gulp-change';
import entities from 'entities';


// Load all Gulp plugins into one variable
const $ = plugins();

// Check for --production flag
const PRODUCTION = !!(yargs.argv.production);

// Load settings from settings.yml
const { PORT, PATHS } = loadConfig();

function loadConfig() {
  let ymlFile = fs.readFileSync('gulpfile.config.yml', 'utf8');
  return yaml.load(ymlFile);
}


// Build the "dist" folder by running all of the below tasks
gulp.task('build',
 gulp.series(clean, gulp.parallel(
   vendor_bower, vendor_composer,
   copy_app_base, copy_app_libs, copy_app_tests, copy_app_public
 )));


// Build the site, run the server, and watch for file changes
gulp.task('default',
  gulp.series('build', server, watch));

gulp.task('watch',
  gulp.series('build', server, watch));



// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {

  var paths = [
    PATHS.dist_app_base,
    PATHS.dist_app_libs,
    PATHS.dist_app_tests,
    PATHS.dist_app_public,
    PATHS.dist_vendor_composer
  ];

  rimraf( '{' + paths.join() + '}', done );
}


// Copy the application files to the app root
function copy_app_base() {
  return gulp.src(PATHS.src_app_base)
    .pipe(gulp.dest(PATHS.dist_app_base));
}
function copy_app_libs() {
  return gulp.src(PATHS.src_app_libs)
    .pipe(gulp.dest(PATHS.dist_app_libs));
}
function copy_app_tests() {
  return gulp.src(PATHS.src_app_tests)
    .pipe(gulp.dest(PATHS.dist_app_tests));
}
function copy_app_public() {
  return gulp.src(PATHS.src_app_public)
    .pipe(gulp.dest(PATHS.dist_app_public));
}



// Copy vendor files into the vendor folder
function vendor_bower() {
  return gulp.src(PATHS.vendor_bower, { base: 'bower_components' })
    .pipe(gulp.dest(PATHS.dist_app + '/vendor'));
}
function vendor_composer() {
  return gulp.src(PATHS.vendor_composer, { base: 'composer_components' })
    .pipe(gulp.dest(PATHS.dist_vendor_composer));
}


// Start a server with BrowserSync to preview the site in
function server(done) {

  /* PHP server which channels through BrowserSync */
  connect.server({
      base: PATHS.dist_app_public,
      port: PORT
    },
    function (){
      browser.init({
        proxy: '127.0.0.1:' + PORT,
        https: true
      });
    }
  );

  done();
}

// Reload the browser with BrowserSync
function reload(done) {
  browser.reload();
  done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watch() {
  gulp.watch(PATHS.src_app + '/**/*.php').on('all', gulp.series(copy_app_base, copy_app_libs, copy_app_tests, copy_app_public, browser.reload));
}
