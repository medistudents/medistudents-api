# Medistudents API Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

Each version should group and class changes in the following format:

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.


## [1.1.0](https://bitbucket.org/medistudents/medistudents-web-ui/branches/compare/1.1.0%0D1.0.0) 2018-07-11

- `Added` This changelog.
- `Added` Favicon.
- `Added` Remote logging through Papertrail.

---

## [1.0.0](https://bitbucket.org/medistudents/medistudents-api/branches/) 2018-07-11

- Up to now!
