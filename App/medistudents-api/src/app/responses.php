<?php
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
* All responses must be pretty-printed JSON.
*/
$app->after(function (Request $request, Response $response) {

  // Set response encoding to pretty-print.
  if( method_exists( $response, 'getEncodingOptions' ) ) {
    $response->setEncodingOptions( $response->getEncodingOptions() | JSON_PRETTY_PRINT );
  }
});
