<?php
// General
$app['app.url'] = 'https://api.medistudents.com';

// Timezone
$app['app.timezone.name'] = 'UTC';
$app['app.timezone.id'] = 38; // UTC in lkup_timezones

// Locale
$app['locale'] = 'en';
$app['session.default_locale'] = $app['locale'];

// Password handling
$app['app.security.algo'] = 'PASSWORD_BCRYPT'; // PHP encryption algo
$app['app.security.encoder'] = 'bcrypt'; // Silex encryption algo
$app['app.security.cost'] = 13; // Currently only used for bcrypt
$app['app.security.salt'] = '2B^~w%Fzy='; // change to invalidate all passwords

$app['app.authorisation.block_length'] = 300; // how long to block a user for due to failed login (secs)
$app['app.authorisation.block_advance'] = 10; // how long to advance the block time due to re-attempts (secs)
$app['app.authorisation.block_attempts'] = 5; // how many login attempts before blocking
$app['app.authorisation.password_reset_valid_for'] = 1800; // how long to advance the password reset expiry (secs)

// Database settings
$app['app.db.host'] = getenv( 'APP_DB_HOST' );
$app['app.db.port'] = getenv( 'APP_DB_PORT' );
$app['app.db.name'] = getenv( 'APP_DB_NAME' );
$app['app.db.user'] = getenv( 'APP_DB_USER' );
$app['app.db.pass'] = getenv( 'APP_DB_PASS' );

// Log file locations
$app['app.logs.app'] = __DIR__ . '/../../logs/app.log';
$app['app.logs.security'] = __DIR__ . '/../../logs/security.log'; // unused at present - everything goes to standard log
$app['app.logs.remote.enable'] = true;
$app['app.logs.remote.prefix'] = 'mediapi';

// Default API settings
$app['app.pagination_per_page'] = 10;

$app['app.user.default_timezone'] = 38; // UTC in lkup_timezones
$app['app.user.default_language'] = 'en_US';
