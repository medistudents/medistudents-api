<?php

$app['debug.verbose'] = true;
$app['debug.info'] = true;


$app['app.security.cost'] = 4; // Currently only used for bcrypt


$app['app.authorisation.block_length'] = 60; // how long to block a user for due to failed login (secs)
$app['app.authorisation.block_advance'] = 1; // how long to advance the block time due to re-attempts (secs)
$app['app.authorisation.block_attempts'] = 3; // how many login attempts before blocking
$app['app.authorisation.password_reset_valid_for'] = 120; // how long to advance the password reset expiry (secs)


$app['app.logs.remote.enable'] = false;
