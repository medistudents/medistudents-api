<?php

/**
* Load the debugger logging service if config allows.
*/
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.name' =>  $app['app.logs.remote.prefix'],
  'monolog.logfile' =>  $app['app.logs.app'],
  'monolog.level' => $app['debug.verbose'] ? 'DEBUG' : ($app['debug.info'] ? 'INFO' : 'WARNING')
));

if( true == $app['app.logs.remote.enable'] ) {
  $app->extend('monolog', function($monolog, $app) {
    
    $monolog->pushHandler( new Monolog\Handler\SyslogUdpHandler(
      $app['app.logs.remote.host'],
      $app['app.logs.remote.port'],
      LOG_USER,
      Monolog\Logger::WARNING
    ));

    return $monolog;
  });
}


/**
* Load the translation service.
*/
use Symfony\Component\Translation\Loader\YamlFileLoader;
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
  'locale_fallbacks' => array('en_US'),
));
$app->extend('translator', function($translator, $app) {

  $lang = 'en_US';

  $translator->addLoader('yaml', new YamlFileLoader());
  foreach( glob(__DIR__ . '/locale/'. $lang . '/*.yml') as $locale ) {
  	$translator->addResource('yaml', $locale, $lang);
  }

  return $translator;
});

/**
* Load the database ORM DBAL service.
*/
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
  'db.options' => array(
    'driver'    => 'pdo_mysql',
    'host'      => $app['app.db.host'],
    'port'      => $app['app.db.port'],
    'dbname'    => $app['app.db.name'],
    'user'      => $app['app.db.user'],
    'password'  => $app['app.db.pass'],
    'charset'   => 'utf8mb4'
  )
));

/**
* Load the validation service.
*/
$app->register(new Silex\Provider\ValidatorServiceProvider());
