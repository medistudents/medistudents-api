<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;


// Load environment variables.
$dotenv = null;
if( file_exists( __DIR__ . "/../.env") ) // production and development servers
  $dotenv = new Dotenv( __DIR__ .'/..' );

else // local development
  $dotenv = new Dotenv( __DIR__ .'/../..' );

$dotenv->load();


$app = new Medistudents\Application();

/**
* Set application-level constants which should never change.
*/
const API_MODEL_VALUE_NOT_UPDATED
  = 'b800b28a-08ea-4281-8f1d-29bf734bde4b'; // UUID to avoid collisions

const API_ACL_MASTER      = 'master';
const API_ACL_STANDARD    = 'standard';
const API_ACL_RESTRICTED  = 'restricted';
const API_ACL_INVALID     = 'invalid';


/**
* Check for required application environment variables.
*/
$env_check_err = "App environment variable %s is not defined.";
$required_env_vars = [
  // App environment
  'APP_ENV',

  // Database
  'APP_DB_HOST', 'APP_DB_PORT', 'APP_DB_NAME',
  'APP_DB_USER', 'APP_DB_PASS',

  'APP_LOGGING_HOST', 'APP_LOGGING_PORT'
];

foreach( $required_env_vars as $var ) {
  if( getenv($var) === false ) {
    printf( $env_check_err, $var );
    exit(1);
  }
}


// Load the config based on the APP_ENV environment variable.
$app['app.env'] = preg_replace('/[^a-z]/', '', getenv('APP_ENV'));
$app['app.logs.remote.host'] = getenv('APP_LOGGING_HOST');
$app['app.logs.remote.port'] = getenv('APP_LOGGING_PORT');

if( !file_exists( __DIR__ . "/config/env_{$app['app.env']}.php") ) {
  echo "Invalid application environment '{$app['app.env']}' defined. Config file does not exist.";
  exit(1);
}

// Load default all-environments config.
require_once( __DIR__ . "/config/default.php" );

// Load environment-specific config. Note that some values defined in the
// default config may be overwritten.
require_once( __DIR__ . "/config/env_{$app['app.env']}.php" );


// Set security settings.
$app['security.default_encoder'] = function ($app) {
  return $app['security.encoder.' . $app['app.security.encoder']];
};
$app['security.encoder.bcrypt.cost'] = $app['app.security.cost'];


// Setting the timezone for date/time functions.
date_default_timezone_set( $app['app.timezone.name'] );

// Load application services.
require_once( __DIR__ . "/services.php" );

// Define application's standard requests.
require_once( __DIR__ . "/requests.php" );

// Define application's standard responses.
require_once( __DIR__ . "/responses.php" );

// Load the application routes.
require_once( __DIR__ . '/routes.php' );

return $app;
