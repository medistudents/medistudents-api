<?php
$app->get('/', function() use ($app) {
  return $app->trans('Welcome to the Medistudents API') . ' [' . $app['app.env'] . ']';
});

$app->get('/ping/', function() use ($app) {
  return $app->json( ['You say PING'=>'I say PONG'], 200 );
});

$app->post('/ping/post/', function() use ($app) {
  return $app->json( ['You\'re posting a PING'=>'I will post a PONG'], 200 );
});

$app->get('/tests/remote-logging/', function() use ($app) {

  $app['monolog']->addDebug( "Testing remote logging - DEBUG" );
  $app['monolog']->addInfo( "Testing remote logging - INFO" );
  $app['monolog']->addWarning( "Testing remote logging - WARNING" );
  $app['monolog']->addError( "Testing remote logging - ERROR" );

  return $app->json( ['Message'=>'Log messages sent'], 200 );
});


$app->mount('/auth', new Medistudents\Controllers\AuthenticateController());

$app->mount('/accounts', new Medistudents\Controllers\AccountsController());

$app->mount('/analytics', new Medistudents\Controllers\AnalyticsController());

$app->mount('/apikeys', new Medistudents\Controllers\ApiKeysController());

$app->mount('/subscriptions', new Medistudents\Controllers\SubscriptionsController());

$app->mount('/subscriptions/plans', new Medistudents\Controllers\SubscriptionPlansController());

$app->mount('/users', new Medistudents\Controllers\UsersController());

return $app;
