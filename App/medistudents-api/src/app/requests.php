<?php
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\ParameterBag,
    Medistudents\Helpers\HttpErrorResponses,
    Medistudents\Helpers\QueryParams,
    Medistudents\Helpers\DataFields,
    Medistudents\Helpers\Utilities,
    Medistudents\Helpers\ApiKeyHelper,
    Medistudents\Models\ApiKey,
    Medistudents\Models\User,
    Medistudents\Models\Account,

    Ramsey\Uuid\Uuid,
    Ramsey\Uuid\Exception\UnsatisfiedDependencyException;


$app->before(function (Request $request) use ($app) {

  $app['remote_ip'] = Utilities::getRemoteIP();

  /**
  * Authenticate access to the API by supplied API key through
  * HTTP Basic Auth.
  */

  // No API key was passed.
  if( !isset($_SERVER['PHP_AUTH_USER']) ) {

    $app['monolog']->addWarning( $app->trans( 'logging.method-message-ip', [
      '%method%'  => '$app->before()',
      '%message%'   => 'No API key passed',
      '%ip_address%' => $app['remote_ip']
    ]));

    header('WWW-Authenticate: Basic realm="Medistudents API"');
    header('HTTP/1.1 401 Unauthorized');

    // Unauthorized
    $http_code = 401;
    $error = HttpErrorResponses::getErr( $http_code );
    return $app->json( $error, $http_code );
  }

  // Check for a matching API Key.
  else {

    $provided_api_key = $_SERVER['PHP_AUTH_USER'];
    $obfuscated_api_key = substr( $provided_api_key, 0, 6 ) . '**************************';


    $checksum_check = ApiKeyHelper::validateApiKey( $provided_api_key ); // length and checkdigit check
    $api_key = $checksum_check ? ApiKey::getByApiKey( $app, $provided_api_key ) : false; // validation check

    if( $api_key ) {
      switch( $api_key->key_type ) {

        case ApiKey::API_KEYTYPE_MASTER :
          $app['authorised.access_level']  = API_ACL_MASTER;
          break;

        case ApiKey::API_KEYTYPE_STANDARD :
          $app['authorised.access_level']  = API_ACL_STANDARD;
          break;

        case ApiKey::API_KEYTYPE_RESTRICTED :
          $app['authorised.access_level']  = API_ACL_RESTRICTED;
          break;

        default :
          $app['authorised.access_level']  = API_ACL_INVALID;
      }
    }

    if( $checksum_check && $api_key
      && ( !empty($app['authorised.access_level'])
          && $app['authorised.access_level'] != API_ACL_INVALID
        )
      )
    {
      $app['authorised.api_key'] = $api_key->api_key;
      $app['authorised.account'] = $api_key->account;
    }
    else {

      if( !$checksum_check ) { // Checksum failed

        $app['monolog']->addWarning( $app->trans( 'logging.method-message-apikey-ip', [
          '%method%'  => '$app->before()',
          '%message%'   => 'API key checksum check failed',
          '%api_key%'   => $obfuscated_api_key,
          '%ip_address%' => $app['remote_ip']
        ]));
      }

      elseif( !$api_key ) {

        $app['monolog']->addWarning( $app->trans( 'logging.method-message-apikey-ip', [
          '%method%'  => '$app->before()',
          '%message%'   => 'API key not found',
          '%api_key%'   => $obfuscated_api_key,
          '%ip_address%' => $app['remote_ip']
        ]));
      }

      elseif( !empty($app['authorised.access_level']) && $app['authorised.access_level'] == API_ACL_INVALID ) {

        $app['monolog']->addWarning( $app->trans( 'logging.method-message-apikey-ip', [
          '%method%'  => '$app->before()',
          '%message%'   => 'API key type invalid',
          '%api_key%'   => $obfuscated_api_key,
          '%ip_address%' => $app['remote_ip']
        ]));
      }

      else { // Other failure

        $app['monolog']->addWarning( $app->trans( 'logging.method-message-apikey-ip', [
          '%method%'  => '$app->before()',
          '%message%'   => 'API key autorisation failed for unknown reason',
          '%api_key%'   => $obfuscated_api_key,
          '%ip_address%' => $app['remote_ip']
        ]));
      }

      // Forbidden
      $http_code = 403;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

  } // else of if( !isset($_SERVER['PHP_AUTH_USER']) )


  /**
  * If a `master` API key is passed, check for a 'super-secret' query parameter which will
  * store the account ID to perform API actions on. If none is passed, retain the account ID
  * which belongs to the API key.
  */
  if( API_ACL_MASTER == $app['authorised.access_level'] ) {

    // Validate pagination page query param if set, if not, set default.
    if( !empty($request->get('account')) && !is_array($request->get('account')) ) {

      $account_id = $request->get('account');

      // Check account exists, if not throw a 404.
      $account_check = Account::getByPKey( $app, $account_id );

      if( !$account_check ) { // No account found

        $app['monolog']->addWarning( $app->trans( 'logging.method-message-account-apikey-ip', [
          '%method%'  => '$app->before()',
          '%message%'   => 'Master API key failed to switch authorized accounts',
          '%account%' => $account_id,
          '%api_key%'   => $obfuscated_api_key,
          '%ip_address%' => $app['remote_ip']
        ]));

        $validation_response = [
          "type" => "invalid",
          "message" => $app->trans( 'Account `%id%` not found when switching authorized accounts', ['%id%' => $account_id] )
        ];
        return $app->json( $validation_response, 422 );
      }

      $app['authorised.account'] = $account_id;
    }
  }

  // Log an attempt to switch to another account on a non-master API key.
  else if( !empty($request->get('account')) && !is_array($request->get('account')) ) {

    $app['monolog']->addWarning( $app->trans( 'logging.method-message-account-apikey-ip', [
      '%method%'  => '$app->before()',
      '%message%'   => 'Failed attempt to switch authorized account on non-Master API key',
      '%account%' => $request->get('account'),
      '%api_key%'   => $obfuscated_api_key,
      '%ip_address%' => $app['remote_ip']
    ]));
  }

  if( empty($app['authorised.account']) ) { // need an account ID stored to use API
    // Unauthorized
    $http_code = 401;
    $error = HttpErrorResponses::getErr( $http_code );
    return $app->json( $error, $http_code );
  }


  // Log that the key was used and on what account
  $app['monolog']->addInfo( $app->trans( 'logging.api-key-authenticated-account', [
    '%api_key%'   => $obfuscated_api_key,
    '%access_level%' => $app['authorised.access_level'],
    '%account%'   => $app['authorised.account']
  ]));


  /**
  * All POST/PUT/PATCH requests to the API must be JSON. Parse if it is,
  * return an error if not.
  *
  * GET/DELETE requests do _not_ have to be JSON.
  */
  $json_requests = array('POST','PUT','PATCH');

  if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
    $data = $request->getContent();

    $app['monolog']->addDebug( 'Request Content:', [$request->getMethod() => $data] );

    if( !empty($data) ) {
      $data = json_decode( $data, true );

      if( json_last_error() != JSON_ERROR_NONE || null === $data ) { // if null is returned by json_decode then invalid content was supplied.

        $validation_response = [
          "type" => "validation_failed",
          "message" => $app->trans( 'validators.invalid-json-data-supplied' )
        ];
        return $app->json( $validation_response, 422 );
      }
    }

    $request->request->replace(is_array($data) ? $data : array());
  }

  // Invalid content type supplied.
  else if( in_array($request->getMethod(), $json_requests) ) {

    $http_code = 415;
    $error = HttpErrorResponses::getErr( $http_code );
    return $app->json( $error, $http_code );
  }


  // Validate pagination page query param if set, if not, set default.
  $request->query->set( 'page', QueryParams::pagination_page( $app, $request->get('page') ) );

  // Validate pagination per_page query param if set, if not, set default.
  $request->query->set( 'per_page', QueryParams::pagination_per_page( $app, $request->get('per_page') ) );

  // Convert embed parameter to array if not null and isn't already.
  $request->query->set( 'embed', QueryParams::options_array( $app, $request->get('embed') ) ) ;

  // Convert sort parameter to array if not null and isn't already.
  $request->query->set( 'sort', QueryParams::options_array( $app, $request->get('sort') ) ) ;

  // Convert options parameter to array if not null and isn't already.
  $request->query->set( 'options', QueryParams::options_array( $app, $request->get('options') ) ) ;

});

/**
* Handle typical application errors.
*/
$app->error(function (\Exception $e, $code) use ($app) {

  // If in debug mode, use nice framework error messages.
  if( $app['debug.info'] ) {
    return;
  }

  $http_code = 400;
  $error = HttpErrorResponses::getErr( $http_code );
  return $app->json( $error, $http_code );
});
