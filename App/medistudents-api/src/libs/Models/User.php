<?php
namespace Medistudents\Models;

use Silex\Application,
    Symfony\Component\Validator\Mapping\ClassMetadata,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\Account;

class User extends AbstractModel {

  const SCHEMA = [// [ db_field,       type,          required,  unsigned/length, unique, default ]
    'id'          => [ 'id',           'bigint',      false,     true,                            ],
    'created_on'  => [ 'created',      'datetime',    true,      null           , false, 'NOW'    ],
    'updated_on'  => [ 'updated',      'datetime',    true,      null           , false, 'NOW'    ],
    'email'       => [ 'email',        'email',       true,      128            , true            ],
    'password'    => [ 'password',     'varchar',     true,      128                              ],
    'is_deleted'  => [ 'is_deleted',   'boolean',     false,     null           , false, 'N'      ],
    'timezone'    => [ 'timezone_id',  'timezone',    true                                        ],
    'language'    => [ 'language_id',  'language',    true,      5                                ],

    'login_success_last'      => [ 'login_success_last',      'datetime',    false                ],
    'login_failure_last'      => [ 'login_failure_last',      'datetime',    false                ],
    'login_failure_count'     => [ 'login_failure_count',     'tinyint',     false                ],
    'login_blocked_until'     => [ 'login_blocked_until',     'datetime',    false                ],

    'password_reset_hash'     => [ 'password_reset_hash',     'varchar',     false,      128      ],
    'password_reset_expiry'   => [ 'password_reset_expiry',   'datetime',    false                ],
    'password_reset_force'    => [ 'password_reset_force',    'boolean',     false,     null    , false, 'N'      ],

    'metadata'                => [ 'metadata',                'keyvalue',    false                ]
  ];

  const API_FIELDS = [
    'id',
    'created_on',
    'updated_on',
    'email',
    'metadata',
    'timezone',
    'language'
  ];

  const EMBED_FIELDS = [
    'timezone' => 'Medistudents\Models\Timezone',
    'language' => 'Medistudents\Models\Language'
  ];

  const META_EMBED_FIELDS = [
    'country' => 'Medistudents\Models\Country'
  ];

  const UPDATABLE_FIELDS = [
    'email',
    'password',
    'metadata',
    'timezone',
    'language'
  ];

  const SORTABLE_FIELDS = [
    'id',
    'created_on',
    'updated_on',
    'email'
  ];

  const TABLE = 'users';
  const P_KEY = 'id';

  const NICE_NAME_SINGLE = 'User';
  const NICE_NAME_PLURAL = 'Users';

  const HAS_DISABLE = false;
  const HAS_SOFT_DELETE = true;
  const HAS_ACCOUNT = false;


  public $id;
  public $created_on;
  public $updated_on;
  public $email;
  public $password;
  public $is_deleted;
  public $timezone;
  public $language;
  public $login_success_last;
  public $login_failure_last;
  public $login_failure_count;
  public $login_blocked_until;
  public $password_reset_hash;
  public $password_reset_expiry;
  public $password_reset_force;
  public $metadata;


  /**
  * Returns whether a user is in a blocked state.
  */
  public function isUserBlocked() {

    return ( !empty( $this->login_blocked_until )
      && $this->login_blocked_until > (new \DateTime("now")) );
  }


  /**
  * Loads a User object by supplied username and password. If returns false then
  * user could not be found based on the u/p or options supplied.
  */
  public static function getByUsernameAndVerifyPassword( Application $app, $username, $password, $options=[] ) {

    // Default options merged with passed values.
    $options = array_merge([
      'allow_deleted' => false
    ], $options );


    $response = [
      'verified' => false,
      'reason' => 'unknown',
      'object' => null
    ];


    $now = new \DateTime('now');

    $username_field = 'email';
    $username = strtolower( trim( $username ) ); // saved usernames (emails) are already lowercase in the DB

    $sql = "SELECT users.* FROM %s AS users %s WHERE 1=1 AND users.$username_field = ? %s LIMIT 1";

    $join_sql = "";
    $where_sql = "";


    if( $options['allow_deleted'] !== true ) {
      $where_sql .= " AND users.is_deleted = 'N' ";
    }

    $sql = sprintf(
      $sql,
      self::TABLE,
      $join_sql,
      $where_sql
    );

    $stmt = $app['db']->executeQuery( $sql, [ $username ] );


    // If no user, state not found and return.
    if( !$result = $stmt->fetch() ) {
      $response['reason'] = 'not_found';
      return $response;
    }

    $user = self::makeFromAssoc( $app, $result );
    $response['object'] = $user;


    // Check if the user is blocked.
    if( $user->isUserBlocked() ) {

      $user->login_blocked_until->add(new \DateInterval('PT'.$app['app.authorisation.block_advance'].'S'));
      $user->login_failure_last = $now;

      $user->dbUpdate();

      $response['reason'] = 'is_blocked';

      return $response;
    }


    // Check if password is verified.
    $password_verified = Utilities::verifyPassword(
      $password,
      $user->password,
      $user->email,
      $app['app.security.salt'] );


    if( !$password_verified ) {

      // If not blocked, need to increment the attempt count then decide whether to block.
      if( (int)$user->login_failure_count < $app['app.authorisation.block_attempts'] ) {
        $user->login_failure_count = (int)$user->login_failure_count + 1;
      }
      else {
        $user->login_blocked_until = $now->add(new \DateInterval('PT'.$app['app.authorisation.block_length'].'S'));
        $user->login_failure_count = 0;
      }

      $user->login_failure_last = $now;
      $user->dbUpdate();

      $response['reason'] = 'password_incorrect';

      return $response;

    }

    // Password VERIFIED.
    else {

      $user->login_success_last = $now;
      $user->login_blocked_until = null;
      $user->login_failure_count = 0;

      $user->dbUpdate();

      $response['verified'] = true;
      $response['reason'] = 'password_verified';

      return $response;
    }

    return false;
  }


  /**
  * Declare validation constraints based on the model's schema.
  */
  static public function loadValidatorMetadata(ClassMetadata $metadata) {

    foreach( self::SCHEMA as $field => $schema_field ) {

      $options = [
        'field_name'     => $field,
        'field_type'     => $schema_field[1],
        'field_required' => isset($schema_field[2]) ? $schema_field[2] : false,
        'field_unsigned' => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false,
        'field_length'   => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false, // same as above
        'field_unique'   => isset($schema_field[4]) ? $schema_field[4] : false,
        'field_default'  => isset($schema_field[5]) ? $schema_field[5] : null
      ];

      $metadata = self::validateFieldRequired( $metadata, $field, $options );
      $metadata = self::validateFieldType( $metadata, $field, $options );

    } // foreach( $class::SCHEMA as $field => $schema_field )
  } // static public function loadValidatorMetadata(ClassMetadata $metadata)

} // class
