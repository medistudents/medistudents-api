<?php
namespace Medistudents\Models;

use Silex\Application,
    Symfony\Component\Validator\Mapping\ClassMetadata;

class Country extends AbstractModel {

  const SCHEMA = [// [ db_field,       type,          required,  unsigned/length, unique, default ]
    'iso'         => [ 'iso',          'char',        true,      2                               ],
    'name'        => [ 'name',         'varchar',     true,      80                               ],
    'nice_name'   => [ 'nice_name',    'varchar',     true,      80                               ],
    'iso3'        => [ 'iso3',         'char',        false,     3                                ],
    'numcode'     => [ 'numcode',      'smallint',    false,     true,                            ],
  ];

  const API_FIELDS = [
    'iso',
    'name',
    'nice_name',
    'iso3',
    'numcode'
  ];

  const EMBED_FIELDS = [];

  const SORTABLE_FIELDS = [
    'iso',
    'name',
    'nice_name',
    'iso3',
    'numcode'
  ];

  const TABLE = 'lkup_countries';
  const P_KEY = 'iso';

  const NICE_NAME_SINGLE = 'Country';
  const NICE_NAME_PLURAL = 'Countries';

  const HAS_DISABLE = false;
  const HAS_SOFT_DELETE = false;
  const HAS_ACCOUNT = false;


  public $id;
  public $name;
  public $nice_name;
  public $iso3;
  public $numcode;



  /**
  * Declare validation constraints based on the model's schema.
  */
  static public function loadValidatorMetadata(ClassMetadata $metadata) {

    foreach( self::SCHEMA as $field => $schema_field ) {

      $options = [
        'field_name'     => $field,
        'field_type'     => $schema_field[1],
        'field_required' => isset($schema_field[2]) ? $schema_field[2] : false,
        'field_unsigned' => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false,
        'field_length'   => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false, // same as above
        'field_unique'   => isset($schema_field[4]) ? $schema_field[4] : false,
        'field_default'  => isset($schema_field[5]) ? $schema_field[5] : null
      ];

      $metadata = self::validateFieldRequired( $metadata, $field, $options );
      $metadata = self::validateFieldType( $metadata, $field, $options );

    } // foreach( $class::SCHEMA as $field => $schema_field )
  } // static public function loadValidatorMetadata(ClassMetadata $metadata)


} // class
