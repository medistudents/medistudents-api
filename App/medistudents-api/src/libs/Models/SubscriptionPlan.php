<?php
namespace Medistudents\Models;

use Silex\Application,
    Symfony\Component\Validator\Mapping\ClassMetadata;

class SubscriptionPlan extends AbstractModel {

  const SCHEMA = [// [ db_field,       type,          required,  unsigned/length, unique, default ]
    'id'          => [ 'id',           'tinyint',     false,     true,                            ],
    'name'        => [ 'name',         'varchar',     true,      80                               ],
    'nice_name'   => [ 'nice_name',    'varchar',     true,      80                               ],
    'cost'        => [ 'cost',         'float',       true                                        ],
    'currency'    => [ 'currency',     'varchar',     true,      3                                ],
    'frequency'   => [ 'frequency',    'varchar',     true,      1                                ]
  ];

  const API_FIELDS = [
    'id',
    'name',
    'nice_name',
    'cost',
    'currency',
    'frequency'
  ];

  const EMBED_FIELDS = [];

  const SORTABLE_FIELDS = [
    'id',
    'name',
    'nice_name',
    'cost',
    'currency',
    'frequency'
  ];

  const TABLE = 'lkup_subscription_plans';
  const P_KEY = 'id';

  const NICE_NAME_SINGLE = 'Subscription Plan';
  const NICE_NAME_PLURAL = 'Subscription Plans';

  const HAS_DISABLE = false;
  const HAS_SOFT_DELETE = false;
  const HAS_ACCOUNT = false;


  public $id;
  public $name;
  public $nice_name;
  public $cost;
  public $currency;
  public $frequency;



  /**
  * Declare validation constraints based on the model's schema.
  */
  static public function loadValidatorMetadata(ClassMetadata $metadata) {

    foreach( self::SCHEMA as $field => $schema_field ) {

      $options = [
        'field_name'     => $field,
        'field_type'     => $schema_field[1],
        'field_required' => isset($schema_field[2]) ? $schema_field[2] : false,
        'field_unsigned' => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false,
        'field_length'   => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false, // same as above
        'field_unique'   => isset($schema_field[4]) ? $schema_field[4] : false,
        'field_default'  => isset($schema_field[5]) ? $schema_field[5] : null
      ];

      $metadata = self::validateFieldRequired( $metadata, $field, $options );
      $metadata = self::validateFieldType( $metadata, $field, $options );

    } // foreach( $class::SCHEMA as $field => $schema_field )
  } // static public function loadValidatorMetadata(ClassMetadata $metadata)


} // class
