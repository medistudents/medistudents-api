<?php
namespace Medistudents\Models;

use Silex\Application,
    Symfony\Component\Validator\Mapping\ClassMetadata;

class AnalyticSubscription extends AbstractModel {

  const SCHEMA = [// [ db_field,       type,          required,  unsigned/length, unique, default ]
    'id'          => [ 'id',           'bigint',      false,     true,                            ],
    'created_on'  => [ 'created',      'datetime',    true,      null           , false, 'NOW'    ],
    'active'      => [ 'active',       'bigint',      false,     true                             ],
    'unpaid'      => [ 'unpaid',       'bigint',      false,     true                             ],
    'trial'       => [ 'trial',        'bigint',      false,     true                             ],
    'cancelled'   => [ 'cancelled',    'bigint',      false,     true                             ]
  ];


  const API_FIELDS = [
    'id',
    'created_on',
    'active',
    'unpaid',
    'trial',
    'cancelled'
  ];

  const EMBED_FIELDS = [];

  const UPDATABLE_FIELDS = [];

  const SORTABLE_FIELDS = [
    'id',
    'created_on'
  ];

  const TABLE = 'analytics_subscriptions';
  const P_KEY = 'id';

  const NICE_NAME_SINGLE = 'Subscription Analytic';
  const NICE_NAME_PLURAL = 'Subscription Analytics';

  const HAS_DISABLE = false;
  const HAS_SOFT_DELETE = false;
  const HAS_ACCOUNT = false;


  public $id;
  public $created_on;
  public $active;
  public $unpaid;
  public $trial;
  public $cancelled;



  /**
  * Declare validation constraints based on the model's schema.
  */
  static public function loadValidatorMetadata(ClassMetadata $metadata) {

    foreach( self::SCHEMA as $field => $schema_field ) {

      $options = [
        'field_name'     => $field,
        'field_type'     => $schema_field[1],
        'field_required' => isset($schema_field[2]) ? $schema_field[2] : false,
        'field_unsigned' => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false,
        'field_length'   => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false, // same as above
        'field_unique'   => isset($schema_field[4]) ? $schema_field[4] : false,
        'field_default'  => isset($schema_field[5]) ? $schema_field[5] : null
      ];

      $metadata = self::validateFieldRequired( $metadata, $field, $options );
      $metadata = self::validateFieldType( $metadata, $field, $options );

    } // foreach( $class::SCHEMA as $field => $schema_field )
  } // static public function loadValidatorMetadata(ClassMetadata $metadata)


} // class
