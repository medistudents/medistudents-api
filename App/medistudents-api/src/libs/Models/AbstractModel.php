<?php
namespace Medistudents\Models;

use Silex\Application,
    Symfony\Component\Validator\Mapping\ClassMetadata,
    Symfony\Component\Validator\Constraints as Assert,
    Doctrine\DBAL\Connection as DBAL,
    Medistudents\Models\Account as Account,
    Medistudents\Helpers\DataFields as DataFields,
    Medistudents\Validators as MedistudentsAssert;

abstract class AbstractModel {

  protected $app;

  public function __construct( Application $app ) {

    $this->app = $app;
    $class = get_called_class();

    // Set the field defaults.
    foreach( $class::SCHEMA as $field => $schema_field ) {

      $field_type = $schema_field[1];
      $field_default  = isset($schema_field[5]) ? DataFields::toResponse( $schema_field[5], $field_type ) : null;

      $this->$field = $field_default;
    }
  }

  /**
  * Declare validation constraints based on the model's schema.
  */
  abstract static public function loadValidatorMetadata(ClassMetadata $metadata);


  /**
  * Adds property contraints for a required field.
  */
  static protected function validateFieldRequired( $metadata, $field, $options ) {

    $field_name     = !empty( $options['field_name'] )     ? $options['field_name'] : null;
    $field_type     = !empty( $options['field_type'] )     ? $options['field_type'] : null;
    $field_required = !empty( $options['field_required'] ) ? $options['field_required'] : null;

    $added_contraint = false;

    if( true == $field_required ) {
      // boolean fields are naturally validated by their type check

      $message = 'validators.required-field-x-missing';
      $payload = [ 'type' => 'missing', $message => ['%name%' => $field_name] ];

      if( 'boolean' != $field_type ) {
        $metadata->addPropertyConstraint( $field, new Assert\NotBlank( [
          'message'=>$message, 'payload'=>$payload ] ) );
      }
      else {
        $metadata->addPropertyConstraint( $field, new Assert\NotNull( [
          'message'=>$message, 'payload'=>$payload ] ) );
      }
    }


    return $metadata;
  } // static protected function validateFieldRequired( $metadata, &$field, $options )

  /**
  * Adds property contraints based on a field's validation criteria.
  */
  static protected function validateFieldType( $metadata, $field, $options=array() ) {

    $field_name     = !empty( $options['field_name'] )     ? $options['field_name'] : null;
    $field_type     = !empty( $options['field_type'] )     ? $options['field_type'] : null;
    $field_required = !empty( $options['field_required'] ) ? $options['field_required'] : false;
    $field_unsigned = !empty( $options['field_unsigned'] ) ? $options['field_unsigned'] : null;
    $field_length   = !empty( $options['field_length'] )   ? $options['field_length'] : null;
    $field_unique   = !empty( $options['field_unique'] )   ? $options['field_unique'] : false;

    /*
    Boolean fields.
    */
    if( $field_type == 'boolean' ) {

      $message = 'validators.boolean-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new Assert\Type( [
        'type' => 'bool', 'message'=>$message, 'payload'=>$payload ] ) );
    }


    /*
    Date-type fields.
    */
    else if( in_array( $field_type, [ 'datetime', 'date', 'timestamp' ] ) ) {

      $message = null;

      switch( $field_type ) {
        case 'datetime'   : $message = 'validators.datetime-value-required-x'; break;
        case 'date'       : $message = 'validators.date-value-required-x'; break;
        case 'timestamp'  : $message = 'validators.timestamp-value-required-x'; break;
      }

      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new Assert\DateTime( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }


    /*
    KeyValue fields.
    */
    else if( $field_type == 'keyvalue' ) {
      $message = 'validators.keyvalue-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new MedistudentsAssert\KeyValue( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }


    /*
    API Key Type fields.
    */
    else if( $field_type == 'keytype' ) {
      $message = 'validators.keytype-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new MedistudentsAssert\KeyType( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }

    /*
    Priority fields.
    */
    else if( $field_type == 'priority' ) {
      $message = 'validators.priority-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new MedistudentsAssert\Priority( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }

    /*
    Device fields.
    */
    else if( $field_type == 'device' ) {
      $message = 'validators.device-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new MedistudentsAssert\Device( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }


    /*
    Timezone fields.
    */
    else if( $field_type == 'timezone' ) {
      $message = 'validators.timezone-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new MedistudentsAssert\Timezone( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }

    /*
    Language fields.
    */
    else if( $field_type == 'language' ) {
      $message = 'validators.language-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new MedistudentsAssert\Language( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }

    /*
    Lat/Long fields.
    */
    else if( $field_type == 'latitude' ) {
      $message = 'validators.latitude-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new MedistudentsAssert\Latitude( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }
    else if( $field_type == 'longitude' ) {
      $message = 'validators.longitude-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new MedistudentsAssert\Longitude( [
        'message'=>$message, 'payload'=>$payload ] ) );
    }


    /*
    Text fields.
    */
    else if( in_array( $field_type, [ 'tinytext','text','mediumtext','longtext','varchar','url','email','identifier' ] ) ) {

      $message = 'validators.text-value-required-x';
      $payload = [ 'type' => 'invalid', $message => ['%name%' => $field_name] ];

      $metadata->addPropertyConstraint( $field, new Assert\Type( [
        'type' => 'string', 'message'=>$message, 'payload'=>$payload ] ) );


      // Validate length
      $max = 65535;

      switch( $field_type ) {
        case 'longtext'   : $max = 4294967295; break;
        case 'mediumtext' : $max = 16777215; break;
        case 'text'       : $max = 65535; break;
        case 'tinytext'   : $max = 255; break;
        case 'identifier' :
        case 'url'        :
        case 'email'      :
        case 'varchar'    : $max = $field_length; break;
      }

      $message = 'validators.text-value-max-x';
      $payload = [
        'type' => 'invalid',
        $message => [ '%name%' => $field_name, '%limit%' => $max ]
      ];

      $metadata->addPropertyConstraint( $field, new Assert\Length(array(
        'max' => $max, 'maxMessage' => $message, 'payload' => $payload
      )));

      // URLs
      if( $field_type == 'url' ) {
        $message = 'validators.url-value-required-x';
        $payload = [ 'type' => 'invalid', $message => [ '%name%' => $field_name ]];

        $metadata->addPropertyConstraint( $field, new Assert\Url( [
          'message' => $message, 'payload' => $payload ] ) );
      }

      elseif( $field_type == 'email' ) {
        $message = 'validators.email-value-required-x';
        $payload = [ 'type' => 'invalid', $message => [ '%name%' => $field_name ]];

        $metadata->addPropertyConstraint( $field, new Assert\Email( [
          'message' => $message, 'payload' => $payload ] ) );
      }

    } // if( in_array( $field_type, [ 'tinytext','text','mediumtext','longtext','varchar','identifier' ] ) )


    /*
    Integer fields.
    */
    else if( in_array( $field_type, [ 'bigint','int','mediumint','smallint','tinyint' ] ) ) {

      $min = 0; $max = 65535;

      if( $field_unsigned ) { // Unsigned values
        switch( $field_type ) {
          case 'bigint' : $max = 18446744073709551615; break;
          case 'int' : $max = 4294967295; break;
          case 'mediumint' : $max = 16777215; break;
          case 'smallint' : $max = 65535; break;
          case 'tinyint' : $max = 255; break;
        }
      }
      else {
        switch( $field_type ) {
          case 'bigint' : $min = -9223372036854775808; $max = 9223372036854775807; break;
          case 'int' : $min = -2147483648; $max = 2147483647; break;
          case 'mediumint' : $min = -8388608; $max = 8388607; break;
          case 'smallint' : $min = -32768; $max = 32767; break;
          case 'tinyint' : $min = -128; $max = 127; break;
        }
      }

      $message_min = 'validators.integer-value-min-x';
      $message_max = 'validators.integer-value-max-x';
      $message_invalid = 'validators.integer-value-invalid-x';

      $payload = [
        'type' => 'invalid',
        $message_min => ['%name%' => $field_name, '%limit%' => $min],
        $message_max => ['%name%' => $field_name, '%limit%' => $max],
        $message_invalid => ['%name%' => $field_name, '%positive%' => ( $min==0 ? 'valid positive' : 'valid' )]
      ];

      $metadata->addPropertyConstraint( $field, new Assert\Range(array(
        'min' => $min, 'max' => $max, 'minMessage' => $message_min, 'maxMessage' => $message_max,
        'invalidMessage' => $message_invalid, 'payload' => $payload
      )));

    } // if( in_array( $field_type, [ 'bigint','int','mediumint','smallint','tinyint' ] ) )


    return $metadata;
  } // static protected function validateFieldType( $metadata, &$field, $options=array() )


  /**
  * Loads a new object of this type by its ID number.
  * @return A loaded object if found, false if not.
  */
  static public function getByPKey( Application $app, $id_arr ) {

    if( !is_array($id_arr) ) {
      $id_arr = array( $id_arr );
    }

    foreach( $id_arr as $k=>$v ) {

      if( false !== filter_var($v, FILTER_VALIDATE_INT,
        ['options' => [ 'default' => false, 'min_range' => 1 ]] ) ) {

        $id_arr[$k] = (int)$v;
      }
    }

    $class = get_called_class();

    $table = $class::TABLE;
    $p_key = $class::P_KEY;
    $has_disable = $class::HAS_DISABLE;
    $has_soft_delete = $class::HAS_SOFT_DELETE;
    $has_account = $class::HAS_ACCOUNT;

    $prepared_sql = '';
    $prepared_stmt_values = [];
    $prepared_stmt_types = [];

    $sql = "
      SELECT tbl.*
      FROM %s AS tbl
      WHERE 1=1
      AND tbl.%s IN ( ? )
      ". ($has_account && $app['authorised.access_level'] !== API_ACL_MASTER ? "AND tbl.account_id = ?" : "" )."
      ". ($has_soft_delete ? "AND tbl.is_deleted = 'N'" : "" )."
      #". ($has_disable ? "AND tbl.is_disabled = 'N'" : "" )."
      LIMIT %u";
    // TODO: [APP] Currently allowing disabled accounts to be accessed (note the commented-out line)
    // - need to decide if field is needed and/or enforced.

    $prepared_sql = sprintf( $sql, $table, $p_key, count($id_arr) );
    $prepared_stmt_values[] = $id_arr;
    $prepared_stmt_types[] = DBAL::PARAM_INT_ARRAY;

    if( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ) {
      $prepared_stmt_values[] = $app['authorised.account'];
      $prepared_stmt_types[] = \PDO::PARAM_INT;
    }

    $stmt = $app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

    $results = array();
    while($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $results[] = $class::makeFromAssoc( $app, $row );
    }

    if( !empty($results) ) {

      if( count($results) == 1 ) {
        return array_pop($results);
      }
      return $results;
    }
    return false;
  }

  /**
  * Loads a new object of this type by its Identifier number.
  * @return A loaded object if found, false if not.
  */
  static public function getByIdentifier( Application $app, $ident_arr, $ident_field='identifier' ) {

    if( !is_array($ident_arr) ) {
      $ident_arr = array( $ident_arr );
    }

    $class = get_called_class();
    $table = $class::TABLE;

    $has_soft_delete = $class::HAS_SOFT_DELETE;
    $has_account = $class::HAS_ACCOUNT;

    $sql = "
      SELECT tbl.*
      FROM %s AS tbl
      WHERE 1=1
      AND tbl.%s IN ( ? )
      ". ($has_account && $app['authorised.access_level'] !== API_ACL_MASTER ? "AND tbl.account_id = ?" : "" )."
      ". ($has_soft_delete ? "AND tbl.is_deleted = 'N'" : "" )."
      LIMIT %u";


    $prepared_sql = sprintf( $sql, $table, $ident_field, count($ident_arr) );
    $prepared_stmt_values[] = $ident_arr;
    $prepared_stmt_types[] = DBAL::PARAM_STR_ARRAY;

    if( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ) {
      $prepared_stmt_values[] = $app['authorised.account'];
      $prepared_stmt_types[] = \PDO::PARAM_STR;
    }

    $stmt = $app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );


    $results = array();
    while($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $results[] = $class::makeFromAssoc( $app, $row );
    }

    if( !empty($results) ) {

      if( count($results) == 1 ) {
        return array_pop($results);
      }
      return $results;
    }
    return false;
  }


  /**
  * Loads embedded items for this model.
  * @return A loaded object if found, false if not.
  */
  static public function getEmbedsByPKey( Application $app, $field, $id ) {

    $class = get_called_class();
    $embed_item = null;

    if( array_key_exists( $field, $class::EMBED_FIELDS ) ) {
      $embed_item = $class::EMBED_FIELDS[$field]::getByPKey( $app, $id );
    }

    elseif( array_key_exists( $field, $class::META_EMBED_FIELDS ) ) {
      $embed_item = $class::META_EMBED_FIELDS[$field]::getByPKey( $app, $id );
    }

    return !empty($embed_item) ? $embed_item : false;
  }


  /**
  * Loads a new object of this type from an associative database object
  * of the same type, correctly converting database field data into its
  * associative data types.
  */
  static public function makeFromAssoc( Application $app, $assoc_arr ) {

    $class = get_called_class();
    $obj = new $class( $app );

    foreach( $class::SCHEMA as $field => $attr)
      $obj->$field = DataFields::toObj( $assoc_arr[ $attr[0] ], $attr[1] );

    return $obj;
  }

  /**
  * Returns true or false as to whether a field value is unique in a table.
  * If the table in question contains an account_id field, then it will
  * only check that the value is unique within the account, _unless_
  * the $full_table_check is set to false.
  */
  public function isUnique( $field, $full_table_check=false ) {

    $class = get_called_class();

    $table = $class::TABLE;
    $db_field = $class::SCHEMA[$field][0];

    $has_soft_delete = $class::HAS_SOFT_DELETE;
    $has_account = $class::HAS_ACCOUNT;


    $sql = "
      SELECT COUNT(*)
      FROM %s AS tbl
      WHERE 1=1
      AND tbl.%s = ?
      ". ($has_account && !$full_table_check ? "AND tbl.account_id = ?" : "" )."
      ". ($has_soft_delete ? "AND tbl.is_deleted = 'N'" : "" )."
      LIMIT 1";


    $prepared_sql = sprintf( $sql, $table, $db_field );
    $prepared_stmt_values[] = $this->$field;
    $prepared_stmt_types[] = \PDO::PARAM_STR; // TODO: [APP] check type

    if( $has_account && !$full_table_check ) {
      $prepared_stmt_values[] = $this->app['authorised.account'];
      $prepared_stmt_types[] = \PDO::PARAM_INT;
    }

    $stmt = $this->app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

    $count = (int)$stmt->fetchColumn();

    return $count <= 0 ? true : false;
  }

  /**
  * Returns an array representation of this model with values converted to
  * values valid to pass to an API response. Will limit returned fields
  * to fields defined by API_FIELDS model attribute.
  *
  * If API_FIELDS is not defined or empty, then this function will return null.
  */
  public function toResponse( $json_encode=false ) {

    $class = get_called_class();

    if( !defined( $class . '::API_FIELDS' ) || empty( $class::API_FIELDS ) )
      return null;


    $response = [];

    foreach( $class::API_FIELDS as $field ) {

      $attr = $class::SCHEMA[ $field ];

      $field_type = $attr[1];
      $response[ $field ] = DataFields::toResponse( $this->$field, $attr[1] );
    }

    return $json_encode ? json_encode($response, JSON_PRETTY_PRINT) : $response;
  }

  /**
  * Returns a JSON representation of the model containing only API fields.
  */
  public function toJson() {
    return $this->toResponse( true );
  }

  /**
  * Returns an array representation of this model containing all fields.
  */
  public function toArray() {

    $class = get_called_class();
    $response = array();

    foreach( $class::SCHEMA as $field => $attr) {
      $field_type = $attr[1];
      $response[ $field ] = $this->$field;
    }

    return $response;
  }

  /**
  * Returns an JSON representation of this model containing all fields.
  */
  public function toString() {
    return json_encode($this->toArray(), JSON_PRETTY_PRINT);
  }

  /**
  * Returns a standard object of this object. This differs from an array
  * in that fields in the schema are converted into their PHP object
  * representations.
  */
  public function toObject() {

    $class = get_called_class();
    $obj = new \stdClass();

    foreach( $class::SCHEMA as $field => $attr)
      $obj->$field = DataFields::toObj( $this->$field, $attr[1] );

    return $obj;
  }


  /**
  * Inserts a new record into the database based on the object's
  * parameter values and schema.
  */
  public function dbInsert() {

    $class = get_called_class();

    $now = new \DateTime();
    $to_db_data = [];

    foreach( $class::SCHEMA as $field => $field_data ) {
      $db_field_name = $field_data[0];
      $db_field_type = $field_data[1];

      $to_db_data[ $db_field_name ] = DataFields::toDb( $this->$field, $db_field_type );
    }

    // Ensure any 'created' values are correctly set to 'now'.
    if( property_exists( $class, 'created_on' ) ) {
      $to_db_data[ $class::SCHEMA['created_on'][0] ] = DataFields::toDb( $now, 'datetime' );
    }

    if( property_exists( $class, 'timestamp' ) ) {
      $to_db_data[ $class::SCHEMA['timestamp'][0] ] = DataFields::toDb( $now, 'datetime' );
    }

    // Ensure any 'updated' values are correctly set to 'now'.
    if( property_exists( $class, 'updated_on' ) ) {
      $to_db_data[ $class::SCHEMA['updated_on'][0] ] = DataFields::toDb( $now, 'datetime' );
    }

    // Clear the primary key for an insert (i.e. object no longer represents
    // a historic object from the database - it's new or at least a failed
    // attempt at creating a new one).
    $this->{$class::P_KEY} = null;
    unset( $to_db_data[ $class::P_KEY ] );

    try {
      $result = $this->app['db']->insert( $class::TABLE, $to_db_data );

      if( 1 >= $result ) { // 1 row affected, therefore has been inserted so get P_KEY value
        $this->{$class::P_KEY} = $this->app['db']->lastInsertId();
        return true;
      }

      return false;
    }
    catch (\Exception $e) {
      $this->app['monolog']->addDebug( $e );
      return false;
    }

  } // public function insert( array $field_values )

  /**
  * Updates a record into the database based on the object's
  * parameter values and schema.
  */
  public function dbUpdate() {

    $class = get_called_class();
    $now = new \DateTime();
    $to_db_data = [];


    // Get the original object from the DB for comparison.
    $original = $class::getByPKey( $this->app, $this->{$class::P_KEY} );


    // Don't want primary key to be altered.
    $this->{$class::P_KEY} = $original->{$class::P_KEY};

    if( false == $original ) {
      $this->app['monolog']->addDebug( 'Could not find original object to update.' );
      return false;
    }

    // Don't want created date to be altered.
    if( property_exists( $class, 'created_on' ) )
      $this->created_on = $original->created_on;

    // Ensure any 'updated' values are correctly set to 'now'.
    if( property_exists( $class, 'updated_on' ) )
      $this->updated_on = DataFields::toResponse( $now, 'datetime' );;



    // Only change fields that are different from original.
    foreach( $class::SCHEMA as $field => $schema_field ) {
      $db_field_name = $schema_field[0];
      $db_field_type = $schema_field[1];

      if( $this->$field !== $original->$field )
        $to_db_data[ $db_field_name ] = DataFields::toDb( $this->$field, $db_field_type );
    }

    try {
      $result = $this->app['db']->update(
        $class::TABLE, $to_db_data, [ $class::P_KEY => $this->{$class::P_KEY} ] );

      return (1 >= $result) ? true : false;
    }
    catch (\Exception $e) {
      $this->app['monolog']->addDebug( $e );
      return false;
    }

  } // public function insert( array $field_values )

}
