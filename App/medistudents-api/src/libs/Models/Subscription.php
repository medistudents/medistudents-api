<?php
namespace Medistudents\Models;

use Silex\Application,
    Symfony\Component\Validator\Mapping\ClassMetadata;

class Subscription extends AbstractModel {

  const SCHEMA = [//      [ db_field,                  type,          required,  unsigned/length, unique, default  ]
    'id'               => [ 'id',                      'bigint',      false,     true                              ],
    'created_on'       => [ 'created',                 'datetime',    true,      null            , false, 'NOW'    ],
    'updated_on'       => [ 'updated',                 'datetime',    true,      null            , false, 'NOW'    ],
    'account'          => [ 'account_id',              'bigint',      true,      true                              ],
    'plan'             => [ 'plan_id',                 'tinyint',      true,     true                              ],
    'status'           => [ 'status',                  'varchar',     true,      1               , false, 'D'      ],
    'tax_percent'      => [ 'tax_percent',             'float',       false,     true            , false, 0        ],
    'discount_percent' => [ 'discount_percent',        'float',       false,     true            , false, 0        ],
    'token'            => [ 'token',                   'varchar',     false,     255                               ],
    'metadata'         => [ 'metadata',                'keyvalue',    false                                        ]
  ];


  const API_FIELDS = [
    'id',
    'created_on',
    'updated_on',
    'account',
    'plan',
    'status',
    'tax_percent',
    'discount_percent',
    'token',
    'metadata'
  ];

  const EMBED_FIELDS = [
    'account' => 'Medistudents\Models\Account',
    'plan' => 'Medistudents\Models\SubscriptionPlan'
  ];

  const UPDATABLE_FIELDS = [
    'account',
    'plan',
    'status',
    'tax_percent',
    'discount_percent',
    'token',
    'metadata'
  ];

  const SORTABLE_FIELDS = [
    'created_on',
    'account',
    'plan',
    'status'
  ];

  const TABLE = 'subscriptions';
  const P_KEY = 'id';

  const NICE_NAME_SINGLE = 'Subscription';
  const NICE_NAME_PLURAL = 'Subscriptions';

  const HAS_DISABLE = false;
  const HAS_SOFT_DELETE = false;
  const HAS_ACCOUNT = true;


  const STATUS_ACTIVE = 'A';
  const STATUS_UNPAID = 'U';
  const STATUS_TRIAL = 'T';
  const STATUS_CANCELLED = 'C';


  public $id;
  public $created_on;
  public $updated_on;
  public $account;
  public $plan;
  public $status;
  public $tax_percent;
  public $discount_percent;
  public $token;
  public $metadata;



  /**
  * Checks whether a supplied status is valid.
  */
  static public function isValidStatus( string $status ) {

    $valid = [
      self::STATUS_ACTIVE,
      self::STATUS_UNPAID,
      self::STATUS_TRIAL,
      self::STATUS_CANCELLED
    ];

    return in_array( $status, $valid ) ? true : false;
  }


  /**
  * Declare validation constraints based on the model's schema.
  */
  static public function loadValidatorMetadata(ClassMetadata $metadata) {

    foreach( self::SCHEMA as $field => $schema_field ) {

      $options = [
        'field_name'     => $field,
        'field_type'     => $schema_field[1],
        'field_required' => isset($schema_field[2]) ? $schema_field[2] : false,
        'field_unsigned' => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false,
        'field_length'   => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false, // same as above
        'field_unique'   => isset($schema_field[4]) ? $schema_field[4] : false,
        'field_default'  => isset($schema_field[5]) ? $schema_field[5] : null
      ];

      $metadata = self::validateFieldRequired( $metadata, $field, $options );
      $metadata = self::validateFieldType( $metadata, $field, $options );

    } // foreach( $class::SCHEMA as $field => $schema_field )
  } // static public function loadValidatorMetadata(ClassMetadata $metadata)


} // class
