<?php
namespace Medistudents\Models;

use Silex\Application,
    Symfony\Component\Validator\Mapping\ClassMetadata;

class Account extends AbstractModel {


  const SCHEMA = [//     [ db_field,        type,          required,  unsigned/length, unique, default ]
    'id'              => [ 'id',            'bigint',      false,     true,                            ],
    'created_on'      => [ 'created',       'datetime',    true,      null           , false, 'NOW'    ],
    'updated_on'      => [ 'updated',       'datetime',    true,      null           , false, 'NOW'    ],
    'is_disabled'     => [ 'is_disabled',   'boolean',     false,     null           , false, 'N'      ],
    'is_deleted'      => [ 'is_deleted',    'boolean',     false,     null           , false, 'N'      ],
    'name'            => [ 'name',          'varchar',     true,      255                              ],
    'recovery_email'  => [ 'recovery_email','email',       true,      128                              ],
    'timezone'        => [ 'timezone_id',   'timezone',    true                                        ],
    'language'        => [ 'language_id',   'language',    true,      5                                ],
    'country'         => [ 'country_iso',   'country',     true                                        ],
    'token'           => [ 'token',         'varchar',     false,     255                              ],
    'metadata'        => [ 'metadata',      'keyvalue',    false                                       ]
  ];

  const API_FIELDS = [
    'id',
    'created_on',
    'updated_on',
    'name',
    'recovery_email',
    'timezone',
    'language',
    'country',
    'token',
    'metadata'
  ];

  const EMBED_FIELDS = [
    'timezone' => 'Medistudents\Models\Timezone',
    'language' => 'Medistudents\Models\Language',
    'country' => 'Medistudents\Models\Country'
  ];

  const UPDATABLE_FIELDS = [
    'name',
    'recovery_email',
    'timezone',
    'language',
    'country',
    'token',
    'metadata'
  ];

  const SORTABLE_FIELDS = [
    'id',
    'created_on',
    'updated_on',
    'name'
  ];

  const TABLE = 'accounts';
  const P_KEY = 'id';

  const NICE_NAME_SINGLE = 'Account';
  const NICE_NAME_PLURAL = 'Accounts';

  const HAS_DISABLE = true;
  const HAS_SOFT_DELETE = true;
  const HAS_ACCOUNT = false;

  public $id;
  public $created_on;
  public $updated_on;
  public $is_disabled;
  public $is_deleted;
  public $name;
  public $recovery_email;
  public $timezone;
  public $language;
  public $country;
  public $token;
  public $metadata;



  /**
  * Declare validation constraints based on the model's schema.
  */
  static public function loadValidatorMetadata(ClassMetadata $metadata) {

    foreach( self::SCHEMA as $field => $schema_field ) {

      $options = [
        'field_name'     => $field,
        'field_type'     => $schema_field[1],
        'field_required' => isset($schema_field[2]) ? $schema_field[2] : false,
        'field_unsigned' => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false,
        'field_length'   => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false, // same as above
        'field_unique'   => isset($schema_field[4]) ? $schema_field[4] : false,
        'field_default'  => isset($schema_field[5]) ? $schema_field[5] : null
      ];

      $metadata = self::validateFieldRequired( $metadata, $field, $options );
      $metadata = self::validateFieldType( $metadata, $field, $options );

    } // foreach( $class::SCHEMA as $field => $schema_field )
  } // static public function loadValidatorMetadata(ClassMetadata $metadata)


} // class
