<?php
namespace Medistudents\Models;

use Silex\Application,
    Symfony\Component\Validator\Mapping\ClassMetadata,
    Medistudents\Models\Account as Account;

class ApiKey extends AbstractModel {

  const SCHEMA = [// [ db_field,       type,          required,  unsigned/length, unique, default ]
    'id'          => [ 'id',           'bigint',      false,     true,                            ],
    'created_on'  => [ 'created',      'datetime',    true,      null           , false, 'NOW'    ],
    'updated_on'  => [ 'updated',      'datetime',    true,      null           , false, 'NOW'    ],
    'key_type'    => [ 'key_type',     'keytype',     false,     1              , false, 'S'      ],
    'is_disabled' => [ 'is_disabled',  'boolean',     false,     null           , false, 'N'      ],
    'name'        => [ 'name',         'varchar',     false,     255                              ],
    'api_key'     => [ 'secret_key',   'identifier',  true,      64             , true            ],
    'account'     => [ 'account_id',   'bigint',      false,     true                             ]
  ];


  const API_FIELDS = [
    'id',
    'created_on',
    'updated_on',
    'name',
    'key_type',
    'api_key',
    'account'
  ];

  const EMBED_FIELDS = [
    'account' => 'Medistudents\Models\Account'
  ];

  const UPDATABLE_FIELDS = [
    'name'
  ];

  const SORTABLE_FIELDS = [
    'id',
    'created_on',
    'updated_on',
    'name',
    'key_type',
    'account'
  ];

  const TABLE = 'accounts_api_keys';
  const P_KEY = 'id';

  const NICE_NAME_SINGLE = 'API Key';
  const NICE_NAME_PLURAL = 'API Keys';

  const HAS_DISABLE = true;
  const HAS_SOFT_DELETE = false;
  const HAS_ACCOUNT = true;


  // Model-specific constants
  const API_KEYTYPE_MASTER     = 'M'; // Master keys are able to edit any account.
  const API_KEYTYPE_STANDARD   = 'S'; // Standard keys are restricted to account-level edits.
  const API_KEYTYPE_RESTRICTED = 'R'; // Restricted keys tend to be read-only.


  public $id;
  public $created_on;
  public $updated_on;
  public $key_type;
  public $is_disabled;
  public $name;
  public $api_key;
  public $account;


  /**
  * Loads an API key object by supplied key. If returns false then
  * key could not be found or the account linked to it is disabled/deleted.
  */
  public static function getByApiKey( Application $app, $api_key ) {

    $api_key = strtoupper( $api_key );
    $sql = "
      SELECT apikeys.*
      FROM %s AS apikeys
      JOIN %s AS accounts ON apikeys.account_id = accounts.id
        AND accounts.is_disabled = 'N'
        AND accounts.is_deleted = 'N'
      WHERE 1=1
      AND apikeys.secret_key = ?
      AND apikeys.is_disabled = 'N'
      LIMIT 1";

    $sql = sprintf(
      $sql,
      ApiKey::TABLE,
      Account::TABLE
    );

    $stmt = $app['db']->executeQuery( $sql, array( $api_key ) );

    if( !$result = $stmt->fetch() )
      return false;

    return ApiKey::makeFromAssoc( $app, $result );
  }



  /**
  * Declare validation constraints based on the model's schema.
  */
  static public function loadValidatorMetadata(ClassMetadata $metadata) {

    foreach( self::SCHEMA as $field => $schema_field ) {

      $options = [
        'field_name'     => $field,
        'field_type'     => $schema_field[1],
        'field_required' => isset($schema_field[2]) ? $schema_field[2] : false,
        'field_unsigned' => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false,
        'field_length'   => isset($schema_field[3]) && !is_null($schema_field[3]) ? $schema_field[3] : false, // same as above
        'field_unique'   => isset($schema_field[4]) ? $schema_field[4] : false,
        'field_default'  => isset($schema_field[5]) ? $schema_field[5] : null
      ];

      $metadata = self::validateFieldRequired( $metadata, $field, $options );
      $metadata = self::validateFieldType( $metadata, $field, $options );

    } // foreach( $class::SCHEMA as $field => $schema_field )
  } // static public function loadValidatorMetadata(ClassMetadata $metadata)


} // class
