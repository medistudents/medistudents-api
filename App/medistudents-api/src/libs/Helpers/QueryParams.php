<?php
namespace Medistudents\Helpers;

use Silex\Application;

/**
* Contains functions to validate query params.
*/
class QueryParams {

  /**
  * Validates pagination per page value, returning default if invalid.
  */
  public static function pagination_per_page( Application $app, $per_page ) {

    $default = (int)$app['app.pagination_per_page'];

    if( false !== filter_var($per_page, FILTER_VALIDATE_INT,
      ['options' => [ 'default' => false, 'min_range' => 1 ]] ) ) {

      return (int)$per_page;
    }

    return $default;

  } // public static function pagination_per_page( Application $app, $per_page )


  /**
  * Validates pagination page value, returning default if invalid.
  */
  public static function pagination_page( Application $app, $page ) {

    $default = 1;

    if( false !== filter_var($page, FILTER_VALIDATE_INT,
      ['options' => [ 'default' => false, 'min_range' => 1 ]] ) ) {

      return (int)$page;
    }

    return $default;

  } // public static function pagination_page( Application $app, $page )


  /**
  * Returns the provided options as an array, also urldecoding them in the process.
  */
  public static function options_array( Application $app, $options ) {

    $options_arr = [];

    if( !is_array($options) ) {
      $options_arr = explode( ',', urldecode( $options ) );
    }

    // Already an array so just decode.
    else {
      foreach( $options as $option ) {
        $options_arr[] = urldecode( $option );
      }
    }

    return !empty( $options_arr ) ? $options_arr : null;
  }

} // class
