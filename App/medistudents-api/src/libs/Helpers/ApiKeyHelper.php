<?php
namespace Medistudents\Helpers;

use Medistudents\Helpers\LuhnModN,
    Ramsey\Uuid\Uuid,
    Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

/**
* Provides methods to generate and validate Medistudents API Keys.
*/
class ApiKeyHelper {

  const BREAK_LENGTH = 5; // Number of characters to insert hyphens between
  const KEY_LENGTH = 39; // length including checkdigit and dashes

  /**
  * Generates a new API Key with included checkdigit.
  */
  static public function generateApiKey() {

    $luhn = new LuhnModN( 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' );

    $key = Uuid::uuid4();
    $key = str_replace('-', '', strtoupper($key->toString()) );

    $checkdigit = $luhn->generateCheckCharacter( $key );

    $key = implode( '-', str_split( $key, self::BREAK_LENGTH ) );
    return $key . $checkdigit;

  } // static public function generateApiKey()


  /**
  * Validates an API key's length and checkdigit.
  */
  static public function validateApiKey( $key ) {

    if( strlen($key) !== self::KEY_LENGTH )
      return false;

    $luhn = new LuhnModN( 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' );
    return $luhn->validateCheckCharacter( str_replace('-', '', $key ) );
  }

} // class
