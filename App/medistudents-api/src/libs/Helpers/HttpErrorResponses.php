<?php
namespace Medistudents\Helpers;

/**
* Contains standardised HTTP error responses in the 400 range.
*
* This excludes 422 however which is a special case for this application
* where it has a unique schema for expanding on the reason for the error.
*
* 500 errors are typically handled by the server (or lack of) so do not
* require these standard error bodies.
*/
class HttpErrorResponses {

  public static function getErr( int $code ) {

    switch( $code ) {
      case 401 : return [ "message" => "Unauthorized" ];
      case 403 : return [ "message" => "Forbidden" ];
      case 404 : return [ "message" => "Not Found" ];
      case 415 : return [ "message" => "Unsupported Media Type" ];
      case 429 : return [ "message" => "Too Many Requests" ];

      case 400 :
      default : return [ "message" => "Bad Request" ];
    }
  }

} // class
