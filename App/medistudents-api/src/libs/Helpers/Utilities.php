<?php
namespace Medistudents\Helpers;

use Medistudents\Helpers\Checksum as Checksum;

class Utilities {

  /**
  * Generates a random identifier string. NOT cryptographically secure.
  * @see stackoverflow.com/questions/307486/short-unique-id-in-php#answer-1516430
  */
  static public function generateRandomString( $length=8, $mixed_case=false ) {

    $chars = $mixed_case
      ? "123456789abcdfghjkmnpqrstvwxyzABCDFGHJKLMNPQRSTVWXYZ"
      : "123456789ABCDFGHJKLMNPQRSTVWXYZ";

    $chars_len = strlen($chars);

    $str = '';
    for($i = 0; $i < $length; $i++) {
      $str .= $chars[ rand(0, $chars_len - 1) ];
    }
    return $str;
  }


  /**
  * Returns a string, salted with two other strings.
  */
  static public function toSalted( $str, $salt1, $salt2 ) {
    return $salt2 . $str . $salt1;
  }

  /**
  * Returns a hashed password
  */
  static public function toPassword( $algo, $str, $salt1=null, $salt2=null, $cost=null ) {

    $salted_pwd = self::toSalted( $str, $salt1, $salt2 );
    $options = is_null($cost) ? [] : ['cost' => $cost];
    $algo = is_string($algo) ?: constant($algo);

    return password_hash( $salted_pwd, $algo, $options );
  }

  /**
  * Verifies a hashed password
  */
  static public function verifyPassword( $password, $hashed_password, $salt1=null, $salt2=null ) {

    $salted_pwd = self::toSalted( $password, $salt1, $salt2 );

    return password_verify( $salted_pwd, $hashed_password );
  }

  /**
  * Takes the last comma or dot (if any) to make a clean float, ignoring
  * thousand separator, currency or any other letter.
  */
  static public function toFloat($num) {

    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

    if(!$sep) {
      return floatval(preg_replace("/[^0-9]/", "", $num));
    }

    return floatval(
      preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
      preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
  }

  /**
  * Returns an anonymous representation of an IP address (i.e. missing last quarter).
  */
  static public function ipAddressAnon() {

    $client_ip = explode( '.', $_SERVER['REMOTE_ADDR'] );

    if( empty($client_ip) || count($client_ip) <= 1 || $client_ip[0] == '::1' ) {
      $client_ip = '127.0.0.x';
    }
    else {
      $client_ip[ (count($client_ip)-1) ] = 'x';
      $client_ip = implode( '.', $client_ip );
    }

    return $client_ip;
  }

  /**
  * Returns a hashed representation of an IP address.
  */
  static public function ipAddressHash() {

    $client_ip = $_SERVER['REMOTE_ADDR'];

    if( empty($client_ip) || $client_ip == '::1' ) {
      $client_ip = '127.0.0.1';
    }

    return self::khash( $client_ip );
  }


  /**
  * Convert crc32 hash to character map.
  * Based upon http://www.php.net/manual/en/function.crc32.php#105703
  */
  static public function khash($data) {

    static $map = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $hash = bcadd(sprintf('%u',crc32($data)) , 0x100000000);
    $str = "";

    do {
      $str = $map[bcmod($hash, 62) ] . $str;
      $hash = bcdiv($hash, 62);
    }
    while ($hash >= 1);
    return $str;
  }

  /**
  * Validates whether a supplied string is JSON.
  */
  static public function isKeyValueJson( $str, $return_decoded_json_on_true=false ) {

    if( empty($str) )
      return false;

    if( substr( $str, 0, 1 ) != '{' )
      return false;


    $decoded = json_decode( $str );

    if( json_last_error() == JSON_ERROR_NONE )
      return $return_decoded_json_on_true ? $decoded : true;

    return false;
  }


  /**
  * Attempts to get the real IP address of a user (it can still be forged through
  * through a proxy).
  */
  static public function getRemoteIP() {

    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
      $ip = $client;

    elseif(filter_var($forward, FILTER_VALIDATE_IP))
      $ip = $forward;

    else
      $ip = $remote;

    return $ip;
  }


} // class Utilities
