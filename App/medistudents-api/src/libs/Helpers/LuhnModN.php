<?php
namespace Medistudents\Helpers;


/**
* Luhn mod N algorithm is an extension to the Luhn algorithm (also known as
* mod 10 algorithm) that allows it to work with sequences of non-numeric
* characters. This can be useful when a check digit is required to validate an
* identification string composed of letters, a combination of letters and digits
* or even any arbitrary set of characters.
*
* @see https://en.wikipedia.org/wiki/Luhn_mod_N_algorithm
*/
class LuhnModN {


  private $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

  /**
  * Constructs a new instance of the class, allowing the alteration of
  * the default alphanumeric alphabet.
  */
  public function __construct( $alphabet=null ) {
    if( !is_null($alphabet) )
      $this->alphabet = $alphabet;
  }

  /**
  * Returns the code-point in the alphabet for a particular character.
  */
  private function codePointFromCharacter( string $character ) {
    return strpos( $this->alphabet, $character );
  }

  /**
  * Returns the character located at a particular code-point in the alphabet.
  */
  private function characterFromCodePoint( int $code_point ) {
    return $this->alphabet[ $code_point ];
  }

  /**
  * Returns the length of the alphabet.
  */
  private function numberOfValidInputCharacters() {
    return strlen( $this->alphabet );
  }



  /**
  * Generates a check character for the given input string.
  */
  public function generateCheckCharacter( string $input ) {

  	$factor = 2;
  	$sum = 0;
  	$n = $this->numberOfValidInputCharacters();

  	// Starting from the right and working leftwards is easier since
  	// the initial "$factor" will always be "2"
  	for( $i = strlen($input) - 1; $i >= 0; $i-- ) {

      $codePoint = $this->codePointFromCharacter( $input[$i] );
  		$addend = $factor * $codePoint;

  		// Alternate the "$factor" that each "$codePoint" is multiplied by
  		$factor = ($factor == 2) ? 1 : 2;

  		// Sum the digits of the "addend" as expressed in base "n"
  		$addend = (int)($addend / $n) + ($addend % $n);
  		$sum += $addend;
  	}

  	// Calculate the number that must be added to the "$sum"
  	// to make it divisible by "n"
  	$remainder = $sum % $n;
  	$checkCodePoint = ($n - $remainder) % $n;

  	return $this->characterFromCodePoint( $checkCodePoint );
  } // public function generateCheckCharacter( string $input )


  /**
  * Validate a string (with the check character as the last character).
  */
  public function validateCheckCharacter( string $input ) {

  	$factor = 1;
  	$sum = 0;
  	$n = $this->numberOfValidInputCharacters();

  	// Starting from the right, work leftwards
  	// Now, the initial "$factor" will always be "1"
  	// since the last character is the check character
  	for( $i = strlen($input) - 1; $i >= 0; $i-- ) {

  		$codePoint = $this->codePointFromCharacter( $input[$i] );
  		$addend = $factor * $codePoint;

  		// Alternate the "$factor" that each "codePoint" is multiplied by
  		$factor = ($factor == 2) ? 1 : 2;

  		// Sum the digits of the "addend" as expressed in base "n"
  		$addend = (int)($addend / $n) + ($addend % $n);
  		$sum += $addend;
  	}

  	$remainder = $sum % $n;

  	return ($remainder == 0);
  } // public function validateCheckCharacter( string $input )

} // class
