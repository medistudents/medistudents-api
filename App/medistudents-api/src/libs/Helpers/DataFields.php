<?php
namespace Medistudents\Helpers;

/**
* Contains standardised functions for DB data fields.
*/
class DataFields {

  /**
  * Returns application datatypes which should be represented to
  * database queries as numerical data.
  */
  public static function numericalTypes() {

    $numerical_types = [
      'bigint', 'int', 'mediumint', 'smallint', 'tinyint',
      'double', 'float',

      'priority', 'device', 'ping_interface',
      'timezone', 'user'
    ];

    return $numerical_types;
  }

  /**
  * Returns whether a data type is numerical or not.
  */
  public static function isNumericalType( $type ) {
    return in_array( $type, self::numericalTypes() );
  }

  /**
  * Will covert a DB field to a simple type or an array. A metadata field
  * will be converted so that it represents a PHP array rather
  * than an encoded JSON array which is what's stored in the database.
  */
  public static function toObj( $data, $type ) {

    switch( $type ) {

      case 'keyvalue' : {
        $data = ( !empty($data) && $data != "" && is_string($data) )
          ? json_decode( $data )
          : null;
        break;
      }

      case 'datetime' :
      case 'timestamp' :
      case 'date' : {
        $data = ( is_string($data) && strtolower($data) == 'now' )
          ? $type == 'datetime' || $type == 'timestamp' ? date( 'Y-m-d H:i:s' ) : date( 'Y-m-d' )
          : $data;

        $data = ( !empty($data) && $data != "" )
          ? new \DateTime( $data )
          : null;

        break;
      }

      case 'string' :
      case 'varchar' :
      case 'char' :
      case 'url' :

      case 'language' : {
        if( empty($data) || trim($data) == "" )
          $data = null;
        break;
      }

      case 'object' : { // May be an int or an array
        $data = ( !empty($data) && $data != "" )
          ? !is_array($data) ? (int)$data : $data
          : null;
        break;
      }

      case 'bigint' : { // separate from other ints incase it needs to be handled as string (32-bit only)
        $data = ( !empty($data) && $data != "" ) ? (int)$data : null; break;
      }

      case 'int' :
      case 'mediumint' :
      case 'smallint' :
      case 'tinyint' : {
        $data = ( !empty($data) && $data != "" ) ? (int)$data : null; break;
      }

      case 'double' :
      case 'latitude' :
      case 'longitude' : {
        $data = ( !empty($data) && $data != "" ) ? (double)$data : null; break;
      }

      case 'float' : {
        $data = ( !empty($data) && $data != "" ) ? (float)$data : null; break;
      }

      case 'boolean' : {

        if( $data === '0' || $data === 0
          || strtoupper(trim($data)) == 'N'
          || strtoupper(trim($data)) == 'NO'
          || strtoupper(trim($data)) == 'FALSE' ) {
          $data = false;
        }

        else if( $data === '1' || $data === 1
          || strtoupper(trim($data)) == 'Y'
          || strtoupper(trim($data)) == 'YES'
          || strtoupper(trim($data)) == 'TRUE' ) {
          $data = true;
        }

        break;
      }

      case 'device' :
      case 'ping_interface' :
      case 'priority' :
      case 'timezone' :
      case 'user' : {
        $data = ( !empty($data) && $data != "" ) ? (int)$data : null;
        break;
      }

    } // switch( $type )

    return $data;
  } // public static function toObj( $data, $type )


  /**
  * Will convert a PHP data type into a value to store in the equivalent
  * database field type. Arrays will be converted to JSON.
  */
  public static function toDb( $data, $type ) {

    $now = new \DateTime();

    $dt_format = function() use ( $type ) {
      switch( $type ) {
        case 'datetime'   : return 'Y-m-d H:i:s';
        case 'timestamp'  : return 'Y-m-d H:i:s';
        case 'date'       : return 'Y-m-d';
      }
    };

    switch( $type ) {

      case 'keyvalue'   :
        $data = is_array( $data ) || is_object( $data )
          ? json_encode( $data )
          : $data;
        break;

      case 'datetime'   :
      case 'date'       :
      case 'timestamp'  :

        if( $data instanceof \DateTime ) {
          $data = $data->format( $dt_format() );
        }
        elseif(is_string( $data ) && strtolower($data) == 'now') {
          $data = $now->format( $dt_format() );
        }
        break;

      case 'boolean' : {

        if( $data === false || $data === '0' || $data === 0
          || strtoupper(trim($data)) == 'NO'
          || strtoupper(trim($data)) == 'FALSE' ) {
          $data = 'N';
        }
        else if( $data === true || $data === '1' || $data === 1
          || strtoupper(trim($data)) == 'YES'
          || strtoupper(trim($data)) == 'TRUE' ) {
          $data = 'Y';
        }

        break;
      }

    } // switch( $type )

    return $data;
  } // public static function toDb( $data, type )


  /**
  * Will convert a PHP data type into a value response field for JSON representation.
  */
  public static function toResponse( $data, $type ) {

    $now = new \DateTime();

    $dt_format = function() use ( $type ) {
      switch( $type ) {
        case 'datetime'   : return 'Y-m-d H:i:s';
        case 'timestamp'  : return 'Y-m-d H:i:s';
        case 'date'       : return 'Y-m-d';
      }
    };

    switch( $type ) {

      case 'keyvalue'   :

        if( is_array($data) && count($data) < 1 ) {
          $data = null; }

        $data = is_array( $data ) || is_object( $data )
          ? json_encode( $data )
          : $data;
        break;

      case 'datetime' :
      case 'date' :
      case 'timestamp' :

        if( $data instanceof \DateTime ) {
          $data = $data->format( $dt_format() );
        }
        elseif(is_string( $data ) && strtolower($data) == 'now') {
          $data = $now->format( $dt_format() );
        }
        break;

      case 'boolean' : {

        if( $data === '0' || $data === 0
          || strtoupper(trim($data)) == 'N'
          || strtoupper(trim($data)) == 'NO'
          || strtoupper(trim($data)) == 'FALSE' ) {
          $data = false;
        }

        else if( $data === '1' || $data === 1
          || strtoupper(trim($data)) == 'Y'
          || strtoupper(trim($data)) == 'YES'
          || strtoupper(trim($data)) == 'TRUE' ) {
          $data = true;
        }

        break;
      }

      // other types don't need conversion to JSON representation

    } // switch( $type )

    return $data;
  } // public static function toDb( $data, type )

} // class
