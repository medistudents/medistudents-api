<?php
namespace Medistudents\Helpers;

/**
* luhnModN (renamed)
*
* This class generates random checksummed strings to be used for unqiue link generation.  The alphabet
* omits confusing characters (like l and 1) and vowels to prevent accidental profanity/word generation.
* This class is not wired up for batch generation or uniquing.
*
* @see https://github.com/MattSurabian/luhn-mod-n
*/
class Checksum {

  protected $alphaLength;
  protected $factor = 2;

  protected $alphabet = 'bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ23456789';

  // Length of finished code, including checksum character.
  protected $codeLength = 20;

  // How many characters after which to insert a hyphen.
  protected $breakLength = 5;

  // Number of characters used to calculate the checksum.
  protected $checksumCalcChars = 9;


  /**
  * Performs alphabet length calculation.
  */
  public function __construct() {
    $this->alphaLength = mb_strlen($this->alphabet);
  }


  /**
  * Takes a code (string) as a parameter and returns a bool to determine if this code passes checksum
  * validation. This is particularly useful to avoid a hit to a data store or such, as if the checksum
  * isn't valid the code can not possibly be valid.
  */
  public function validateChecksum($code) {

    $code = $this->removeBreaks( $code );

    // If the code isn't the right length, it's not valid.
    if( !isset( $code{ $this->codeLength-1 } ) ) {
      return false;
    }

    $checkSum = $this->generateCheckSum( $this->removeChecksum($code) );
    $checkCode = $this->attachChecksum( $this->removeChecksum($code), $checkSum );

    if( $checkCode === $code )
      return true;

    return false;
  } // public function validateChecksum($code)


  /**
  * Provides the logic for string generation.
  */
  public function generateString() {

    $code = "";
    $length = $this->codeLength - 1;

    //checking the string key is faster than checking string length
    while( !isset( $code{ $length-1 } ) ) {
      $code .= $this->alphabet{ mt_rand( 0, $this->alphaLength - 1 ) };
    }

    $code = $this->attachChecksum( $code, $this->generateCheckSum($code) );
    $code = $this->insertBreaks( $code );

    return $code;
  }


  /**
  * Called by generateString to perform necessary checksum calculations, returns checksum character.
  * @param $code
  * @return string
  */
  public function generateCheckSum( $code ) {

    $sum = 0;
    $codeLen = mb_strlen($code);
    $curFactor = $this->factor;

    for($i = $codeLen-1; $i >= ($codeLen-$this->checksumCalcChars); $i--) {

      $num = $i;
      $codePoint = strpos($this->alphabet, $code{$num});
      $addend = $curFactor * $codePoint;

      // Alternate the factor that each point is multiplied by.
      $curFactor = ($curFactor == $this->factor) ? 1 : $this->factor;
      $addend = ($addend/$this->alphaLength) + ($addend%$this->alphaLength);
      $sum += $addend;
    }

    $remainder = $sum % $this->alphaLength;
    $checkCodePoint = ($this->alphaLength-$remainder) % $this->alphaLength;

    return $this->alphabet{$checkCodePoint};

  } // public function generateCheckSum($code)



  /**
  * Used by generateString to attach the checksum to the code. This helper method is useful in the event
  * you want to change where the checksum is placed. By default the checksum is appended to the code.
  * @param $code
  * @param $checksum
  * @return string
  */
  protected function attachChecksum( $code, $checksum ) {
    return $code.$checksum;
  }


  /**
  * Removes the checksum. This is utilized when validating a code
  * @param $code
  * @return string
  */
  protected function removeChecksum( $code ) {
    return substr( $code, 0, -1 );
  }

  /**
  * Inserts break hyphens into supplied code.
  */
  protected function insertBreaks( $code ) {
    if( empty($this->breakLength) )
      return $code;

    $split = str_split( $code, $this->breakLength );
    $code = implode( '-', $split );

    return $code;
  }

  /**
  * Removes break hyphens from supplied code.
  */
  protected function removeBreaks( $code ) {
    return str_replace( '-', '', $code );
  }

}
