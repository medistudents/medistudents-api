<?php
namespace Medistudents;

use Silex\Application as SilexApplication;

class Application extends SilexApplication {

  use SilexApplication\MonologTrait;
  use SilexApplication\TranslationTrait;
}
