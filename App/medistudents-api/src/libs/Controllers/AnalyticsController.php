<?php
namespace Medistudents\Controllers;

use Silex\Application,
    Silex\Api\ControllerProviderInterface,
    Symfony\Component\HttpFoundation\Request AS Request,
    Symfony\Component\HttpFoundation\Response AS Response,
    Symfony\Component\HttpFoundation\JsonResponse,
    Medistudents\Helpers\HttpErrorResponses AS HttpErrorResponses,
    Medistudents\Helpers\DataFields AS DataFields,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\Account AS Account,
    Medistudents\Models\Subscription AS Subscription,
    Medistudents\Models\AnalyticSubscription AS AnalyticSubscription;

class AnalyticsController extends AbstractDataController {

  const MOUNT_POINT = '/analytics';


  /**
  * Define routes.
  */
  public function connect( Application $app ) {

    $controllers = $app['controllers_factory'];

    // Adds a new analytic line for subscriptions.
    $controllers->post( '/subscriptions/', __CLASS__.'::addAnalyticSubscription' );


    return $controllers;
  }




  /**
  * Adds an item to the database.
  */
  public function addAnalyticSubscription( Application $app, Request $request ) {

    /**
    * This is a API Master Key access-only endpoint.
    */
    if( API_ACL_MASTER != $app['authorised.access_level'] ) {
      $http_code = 403; // Forbidden
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    $class = get_called_class();
    $analytic_model = 'Medistudents\Models\AnalyticSubscription';
    $analytic_item = new $analytic_model( $app );

    $subscription_item = new Subscription( $app );

    $headers = [];
    $action = $app->trans('add');
    $action_ing = $app->trans('adding');
    $action_on = $app->trans( $analytic_item::NICE_NAME_SINGLE );



    //-- Retrieve the analytics from the database using efficient query.

    $prepared_sql = '';
    $prepared_stmt_values = [];
    $prepared_stmt_types = [];


    $sql = "
      SELECT COUNT(*) AS count
      FROM %s AS tbl
      JOIN (
         SELECT MAX(%s) as id
         FROM %s
         GROUP BY %s
      ) tbl2 ON tbl2.id = tbl.id
      INNER JOIN %s AS tbl3 ON tbl.%s = tbl3.%s
      WHERE 1=1
      AND tbl3.%s = 'N'
      AND tbl3.%s = 'N'
      AND tbl.%s = ?;
    ";

    $prepared_sql = sprintf(
      $sql,
      // From
      Subscription::TABLE,
      // Join
      Subscription::P_KEY,
      Subscription::TABLE,
      Subscription::SCHEMA['account'][0],
      // Inner Join
      Account::TABLE,
      Subscription::SCHEMA['account'][0],
      Account::P_KEY,
      // Where
      Account::SCHEMA['is_disabled'][0],
      Account::SCHEMA['is_deleted'][0],
      Subscription::SCHEMA['status'][0]
    );


    $subscription_analytics = [
      Subscription::STATUS_ACTIVE => 0,
      Subscription::STATUS_UNPAID => 0,
      Subscription::STATUS_TRIAL => 0,
      Subscription::STATUS_CANCELLED => 0
    ];

    foreach( $subscription_analytics as $k=>$v ) {

      $prepared_stmt_values = [ $k ];
      $prepared_stmt_types = [ \PDO::PARAM_STR ];

      $stmt = $app['db']->executeQuery(
        $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

      $subscription_analytics[$k] = intval( $stmt->fetchColumn() );
    }



    //-- Write gained analytic values to the database.

    $analytic_item->active    = $subscription_analytics[Subscription::STATUS_ACTIVE];
    $analytic_item->unpaid    = $subscription_analytics[Subscription::STATUS_UNPAID];
    $analytic_item->trial     = $subscription_analytics[Subscription::STATUS_TRIAL];
    $analytic_item->cancelled = $subscription_analytics[Subscription::STATUS_CANCELLED];


    try {
      if( $analytic_item->dbInsert() ) {

        // Reload the item.
        $analytic_item = $analytic_model::getByPKey( $app, $analytic_item->{$analytic_model::P_KEY} );

        // Add Location header to point to the newly created resource.
        $link_format = $app['app.url'] . $class::MOUNT_POINT . '/subscriptions/%s/';
        $headers['Location'] = sprintf( $link_format, $analytic_item->{$analytic_model::P_KEY} );

        return $app->json( $analytic_item->toResponse(), 201, $headers );
      }
    }
    catch( \Exception $e ) {
      if( $e->getCode() == 422 ) { // validation errors

        $response = [
          'type' => 'validation_failed',
          'code' => 'errors_found',
          'message' => $app->trans('Validation errors found when %action% %name%',
            ['%action%' => $action_ing, '%name%' => $action_on] ),
          'issues' => $analytic_item->getErrors()
        ];

        return $app->json( $response, 422 );
      }
    }

    // At this point, non-addition is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => $action, '%name%' => $action_on]), 500);

  } // public function addAnalyticSubscription()


} // class
