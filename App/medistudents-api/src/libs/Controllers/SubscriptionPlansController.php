<?php
namespace Medistudents\Controllers;

use Silex\Application,
    Silex\Api\ControllerProviderInterface,
    Symfony\Component\HttpFoundation\Request AS Request,
    Symfony\Component\HttpFoundation\Response AS Response,
    Symfony\Component\HttpFoundation\JsonResponse,
    Medistudents\Helpers\HttpErrorResponses AS HttpErrorResponses,
    Medistudents\Helpers\DataFields AS DataFields,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\SubscriptionPlan AS SubscriptionPlan;

class SubscriptionPlansController extends AbstractDataController {

  const MODEL  = 'Medistudents\Models\SubscriptionPlan';
  const MOUNT_POINT = '/subscriptions/plans';


  /**
  * Define routes.
  */
  public function connect( Application $app ) {

    $controllers = $app['controllers_factory'];

    // Return item for an account.
    $controllers->get( '/', __CLASS__.'::getMany' );

    // Retrieves a single item for an account.
    $controllers->get( '/{id}/', __CLASS__.'::getOneItem' );


    return $controllers;
  }

  /**
  * Retrieves a single item.
  */
  public function getOneItem( Application $app, $id ) {

    // If a non-int is passed, then retrieve subscription plan by 'name' identifier field.
    if( false === filter_var($id, FILTER_VALIDATE_INT, ['options' => [ 'default' => false, 'min_range' => 0 ]] ) ) {
      return $this->getOne( $app, $id, [ 'identifier'=>'name' ] );
    }

    return $this->getOne( $app, $id );
  }
} // class
