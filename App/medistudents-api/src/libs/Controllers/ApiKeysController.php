<?php
namespace Medistudents\Controllers;

use Silex\Application,
    Silex\Api\ControllerProviderInterface,
    Symfony\Component\HttpFoundation\Request AS Request,
    Symfony\Component\HttpFoundation\Response AS Response,
    Symfony\Component\HttpFoundation\JsonResponse,
    Medistudents\Helpers\ApiKeyHelper AS ApiKeyHelper,
    Medistudents\Helpers\HttpErrorResponses AS HttpErrorResponses,
    Medistudents\Helpers\DataFields AS DataFields,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\ApiKey;

class ApiKeysController extends AbstractDataController {

  const MODEL  = 'Medistudents\Models\ApiKey';
  const MOUNT_POINT = '/apikeys';


  /**
  * Define routes.
  */
  public function connect( Application $app ) {

    $controllers = $app['controllers_factory'];

    // Return items for an account.
    $controllers->get( '/', __CLASS__.'::getMany' );

    // Retrieves a single item for an account.
    $controllers->get( '/{id}/', __CLASS__.'::getOne' );


    // Adds a new item to an account.
    $controllers->post( '/', __CLASS__.'::addOne' );

    // Updates a item on an account.
    $controllers->patch( '/{id}/', __CLASS__.'::updateOne' );

    // Toggles a single item's disabled status.
    $controllers->patch( '/{id}/disable/', __CLASS__.'::disableOneItem' );


    return $controllers;
  }


  /**
  * Updates an item in the database.
  */
  public function updateOne( Application $app, Request $request, $id ) {

    $model = $this::MODEL;
    return $this->updateOneItem( $app, $request, $id, $model::UPDATABLE_FIELDS );
  }


  /**
  * Adds an item to the database.
  */
  public function addOne( Application $app, Request $request ) {

    $model = $this::MODEL;
    $item = new $model( $app );

    //- START Set values from API call ----------------------------------------

    $item->name        = $request->get('name');
    $item->api_key     = ApiKeyHelper::generateApiKey();

    $request->query->set( 'key_type', ( !empty($request->get('key_type'))
      ? $request->get('key_type')
      : ApiKey::API_KEYTYPE_RESTRICTED
    ) );

    $item->key_type = $request->get('key_type');


    //- END Set values from API call ------------------------------------------


    return $this->addOneItem( $app, $request, $item );
  }



} // class
