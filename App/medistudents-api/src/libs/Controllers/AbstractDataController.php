<?php
namespace Medistudents\Controllers;

use Silex\Application,
    Silex\Api\ControllerProviderInterface,
    Symfony\Component\HttpFoundation\Request AS Request,
    Symfony\Component\HttpFoundation\Response AS Response,
    Symfony\Component\HttpFoundation\JsonResponse,
    Medistudents\Helpers\HttpErrorResponses AS HttpErrorResponses,
    Medistudents\Helpers\DataFields AS DataFields,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\Account AS Account;

abstract class AbstractDataController implements ControllerProviderInterface {


  /**
  * Return items for an account.
  */
  public function getMany( Application $app, Request $request, $options=[] ) {

    // Set default options if not already set.
    if( !array_key_exists('format', $options) )
      $options['format'] = 'default';

    if( !array_key_exists('joins', $options) )
      $options['joins'] = null;

    if( !array_key_exists('query_extra', $options) )
      $options['query_extra'] = null;


    $class = get_called_class();
    $model = isset( $options['model'] ) ? $options['model'] : $class::MODEL;

    /**
    * If model is 'account', disable access to master keys and authorised accounts.
    */
    if( $model == 'Medistudents\Models\Account' &&
        API_ACL_MASTER != $app['authorised.access_level'] ) {

      $http_code = 403; // Forbidden
      $error = HttpErrorResponses::getErr( $http_code );

      if( $options['format'] == 'objects' )
        return false;

      return $app->json( $error, $http_code );
    }


    $table = $model::TABLE;
    $p_key = $model::P_KEY;
    $has_soft_delete = $model::HAS_SOFT_DELETE;
    $has_account = $model::HAS_ACCOUNT;

    $headers = [];


    //
    // Define the order of items to return from the database.
    $sort_requests = $app['request_stack']->getCurrentRequest()->get('sort');

    if( !empty($model::SORTABLE_FIELDS) && !empty($sort_requests) && is_array($sort_requests) ) {
      $first_order_by = true;

      foreach( $sort_requests as $sort_fld ) {

        $asc = strpos( $sort_fld, '-' ) ? false : true;
        $sort_fld = str_replace( '-', '', $sort_fld );

        if( in_array( $sort_fld, $model::SORTABLE_FIELDS ) ) {

          if( $first_order_by ) {
            $options['query_extra'] .= " ORDER BY ";
            $first_order_by = false;
          }
          else {
            $options['query_extra'] .= ",";
          }

          $options['query_extra'] .= "
            tbl." . $model::SCHEMA[$sort_fld][0] . ($asc ? " ASC" : " DESC");
        }
      }
    }


    //
    // Retrieve all the items.

    $prepared_sql = '';
    $prepared_stmt_values = [];
    $prepared_stmt_types = [];

    $sql = "
      SELECT tbl.*
      FROM %s AS tbl
      ". $options['joins'] . "
      WHERE 1=1
      ". ($has_account && $app['authorised.access_level'] !== API_ACL_MASTER ? "AND tbl.account_id = ?" : "" )."
      ". ($has_soft_delete ? "AND tbl.is_deleted = 'N'" : "" )."
      ". $options['query_extra'];

    if( !isset( $options['limit'] ) || $options['limit'] != 'no_limit' ) {
      $sql .= "
      LIMIT %d,%d";
    }

    $per_page = $app['request_stack']->getCurrentRequest()->get('per_page');
    $page = $app['request_stack']->getCurrentRequest()->get('page');

    $offset = ($page - 1) * $per_page;

    if( !isset( $options['limit'] ) || $options['limit'] != 'no_limit' ) {
      $prepared_sql = sprintf( $sql, $table, $offset, $per_page );
    }
    else {
      $prepared_sql = sprintf( $sql, $table );
    }

    if( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ) {
      $prepared_stmt_values[] = $app['authorised.account'];
      $prepared_stmt_types[] = \PDO::PARAM_INT;
    }

    $app['monolog']->addDebug( "SQL:" . $prepared_sql );

    $stmt = $app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

    $result = $stmt->fetchAll();


    // If no result was found, return a standard 404.
    if( empty( $result ) ) {

      if( $options['format'] == 'objects' )
        return false;

      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    // If a result was returned, convert it into the correct schema and return.
    $items = [];

    foreach( $result as $d ) {
      $item = $model::makeFromAssoc( $app, $d );


      // Retrieve embedded objects.
      $embed_requests = $app['request_stack']->getCurrentRequest()->get('embed');

      if( method_exists( $item, 'getEmbedsByPKey' ) && !empty($embed_requests) && is_array($embed_requests) ) {

        foreach( $embed_requests as $k => $embed_fld ) {

          if( property_exists($model, $embed_fld) ) {

            $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

            if( !empty($embed_item) ) {
              $item->$embed_fld = $embed_item->toResponse();
            }
            else {
              $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->$embed_fld );
            }
          }

          elseif( isset( $item->metadata->$embed_fld ) ) {

            $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->metadata->$embed_fld );

            if( !empty($embed_item) ) {
              $item->metadata->$embed_fld = $embed_item->toResponse();
            }
            else {
              $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->metadata->$embed_fld );
            }
          }
        }
      }


      if( $options['format'] == 'objects' )
        $items[] = $item;
      else
        $items[] = $item->toResponse();

    }


    // Get the pagination and total count headers.
    if( $options['format'] != 'objects' )
      $headers = array_merge( $headers, $this->getPaginationHeaders($app, $per_page, $page, $options) );


    // Return to GeoJSON objects if requested.
    if( $options['format'] == 'geojson' && !empty($model::CAN_FORMAT_GEOJSON) ) {

      $collection_geojson = [];

      foreach( $items as $d ) {

        $item_geojson = [
          "id" => $d[$item::P_KEY],
          "type" => "Feature",
          "properties" => $d,
        ];

        $item_geojson['properties']['latest'] = true; // TODO: Need to check which one is the first record and mark it

        if( !empty($d['latitude']) && !empty($d['longitude']) ) {

          $item_geojson["geometry"] = [
            "type" => "Point",
            "coordinates" => [ $d['latitude'], $d['longitude'] ]
          ];
        }

        $collection_geojson[] = $item_geojson;
      }

      $collection = [
        "type" => "FeatureCollection",
        "features" => $collection_geojson
      ];

      return $app->json( $collection, 200, $headers );
    }


    if( $options['format'] == 'objects' )
      return $items;

    return $app->json( $items, 200, $headers );

  } // public function getMany( Application $app )


  /**
  * Retrieves a single item based on the ID or an identifier (if model has one).
  * Can also retrieve items linked by use of embedding.
  */
  public function getOne( Application $app, $id, $options=[] ) {

    // Set default options if not already set.
    if( !array_key_exists('format', $options) ) {
      $options['format'] = 'default';
    }

    $class = get_called_class();
    $model = $class::MODEL;

    /**
    * If model is 'account', disable access to master keys and authorised accounts.
    */
    if( $model == 'Medistudents\Models\Account' &&
        $id != $app['authorised.account'] &&
        API_ACL_MASTER != $app['authorised.access_level'] ) {

      $http_code = 403; // Forbidden
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    $headers = [];
    $item = null;


    // Check to see if an ID number was passed (integer)
    if( false !== filter_var($id, FILTER_VALIDATE_INT, ['options' => [ 'default' => false, 'min_range' => 0 ]] ) ) {
      $item = $model::getByPKey( $app, $id );
    }

    // Else a string identifier (or invalid int) was passed
    elseif( method_exists( $model, 'getByIdentifier' ) ) {

      if( !empty($options['identifier']) )
        $item = $model::getByIdentifier( $app, $id, $options['identifier'] );
      else
        $item = $model::getByIdentifier( $app, $id );
    }


    // If no result was found, return a standard 404.
    if( empty($item) ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    // Retrieve embedded objects.
    $embed_requests = $app['request_stack']->getCurrentRequest()->get('embed');

    if( method_exists( $item, 'getEmbedsByPKey' ) && !empty($embed_requests) && is_array($embed_requests) ) {

      foreach( $embed_requests as $k => $embed_fld ) {

        if( property_exists($model, $embed_fld) ) {

          $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

          if( !empty($embed_item) ) {
            $item->$embed_fld = $embed_item->toResponse();
          }
          else {
            $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->$embed_fld );
          }
        }

        elseif( isset( $item->metadata->$embed_fld ) ) {

          $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->metadata->$embed_fld );

          if( !empty($embed_item) ) {
            $item->metadata->$embed_fld = $embed_item->toResponse();
          }
          else {
            $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->metadata->$embed_fld );
          }
        }
      }
    }

    // Return to GeoJSON object if requested.
    if( $options['format'] == 'geojson' && !empty($model::CAN_FORMAT_GEOJSON) ) {

      $item_geojson = [
        "id" => $item->{$item::P_KEY},
        "type" => "Feature",
        "properties" => $item->toResponse(),
      ];

      if( !empty($item->latitude) && !empty($item->longitude) ) {

        $item_geojson["geometry"] = [
          "type" => "Point",
          "coordinates" => [ $item->latitude, $item->longitude ]
        ];
      }

      return $app->json( $item_geojson, 200, $headers );
    }

    return $app->json( $item->toResponse(), 200, $headers );
  } // public function getOne(Application $app, $id)


  /**
  * Hard deletes, i.e. removes the table entry, of an item from the database.
  */
  public function deleteOneHard( Application $app, $id ) {

    $class = get_called_class();
    $model = $class::MODEL;

    /**
    * If model is 'account', disable access to master keys and authorised accounts.
    */
    if( $model == 'Medistudents\Models\Account' &&
        $id != $app['authorised.account'] &&
        API_ACL_MASTER != $app['authorised.access_level'] ) {

      $http_code = 403; // Forbidden
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    /**
    * Validate that ID is an integer.
    */
    if( false === filter_var($id, FILTER_VALIDATE_INT, ['options' => [ 'default' => false, 'min_range' => 0 ]] ) ) {

      $data_validation_errors = [[
        'param' => 'id',
        'code' => 'invalid',
        'message' => $app->trans( 'validators.integer-value-invalid-x', ['%name%'=>'id'] )
      ]];

      $validation_response = [
        'type' => 'validation_failed',
        'code' => 'errors_found',
        'message' => $app->trans('Validation errors found when %action% %name%',
          ['%action%' => 'updating', '%name%' => 'User'] ),
        'issues' => $data_validation_errors
      ];

      return $app->json( $validation_response, 422 );
    }


    $table = $model::TABLE;

    $has_account = $model::HAS_ACCOUNT;

    $prepared_stmt_values = [];
    $prepared_stmt_types = [];

    // Count the rows first
    $sql = "
      SELECT COUNT(*)
      FROM %s AS tbl
      WHERE tbl.id = ?
      ". ( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ? "AND tbl.account_id = ?" : "");

    $prepared_sql = sprintf( $sql, $table );

    $prepared_stmt_values[] = $id;
    $prepared_stmt_types[] = \PDO::PARAM_INT;

    if( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ) {
      $prepared_stmt_values[] = $app['authorised.account'];
      $prepared_stmt_types[] = \PDO::PARAM_INT;
    }

    $stmt = $app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

    $count_all = (int)$stmt->fetchColumn();


    if( $count_all < 1 ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    // Now delete

    $sql = "
      DELETE tbl
      FROM %s AS tbl
      WHERE tbl.id = ?
      ". ( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ? "AND tbl.account_id = ?" : "");

    $prepared_sql = sprintf( $sql, $table );

    $result = $app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

    if( $result ) {

      // If here then $result will store the number of records affected (hopefully 1)
      $response = [
        'message' => $app->trans('%name% was successfully %action%',
           ['%action%' => 'deleted', '%name%' => $model::NICE_NAME_SINGLE ])
      ];
      return $app->json( $response, 200 );
    }

    // At this point, non-addition is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => 'delete', '%name%' => $model::NICE_NAME_SINGLE]), 500);

  } // public function deleteOneHard( Application $app, $id )


  /**
  * Soft deletes an item from the database by setting is_deleted flag to true.
  */
  public function deleteOneSoft( Application $app, $id ) {

    $class = get_called_class();
    $model = $class::MODEL;

    /**
    * If model is 'account', disable access to master keys and authorised accounts.
    */
    if( $model == 'Medistudents\Models\Account' &&
        $id != $app['authorised.account'] &&
        API_ACL_MASTER != $app['authorised.access_level'] ) {

      $http_code = 403; // Forbidden
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    $table = $model::TABLE;

    $has_account = $model::HAS_ACCOUNT;
    $has_soft_delete = $model::HAS_SOFT_DELETE;


    if( !$has_soft_delete ) {
      $validation_response = [
        "type" => "invalid",
        "message" => $app->trans( 'validators.soft-delete-unavailable' )
      ];
      return $app->json( $validation_response, 422 );
    }


    $prepared_stmt_values = [];
    $prepared_stmt_types = [];

    // Count the rows first
    $sql = "
      SELECT COUNT(*)
      FROM %s AS tbl
      WHERE tbl.id = ?
      ". ( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ? "AND tbl.account_id = ?" : "");

    $prepared_sql = sprintf( $sql, $table );

    $prepared_stmt_values[] = $id;
    $prepared_stmt_types[] = \PDO::PARAM_INT;

    if( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ) {
      $prepared_stmt_values[] = $app['authorised.account'];
      $prepared_stmt_types[] = \PDO::PARAM_INT;
    }

    $stmt = $app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

    $count_all = (int)$stmt->fetchColumn();


    if( $count_all < 1 ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    // Now update

    $set = [
      'is_deleted' => DataFields::toDb( true, 'boolean' ),
      'updated' => DataFields::toDb( new \DateTime(), 'datetime' )
    ];

    $where = [];
    $where['is_deleted'] = DataFields::toDb( false, 'boolean' );

    if( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ) {
      $where['account_id'] = $app['authorised.account'];
    }


    // Check to see if an ID number was passed (integer)
    if( false !== filter_var($id, FILTER_VALIDATE_INT, ['options' => [ 'default' => false, 'min_range' => 0 ]] ) )
      $where[ $model::P_KEY ] = $id;

    else // Else a string identifier (or invalid int) was passed
      $where['identifier'] = $id;

    $result = $app['db']->update( $model::TABLE, $set, $where );

    // If no rows were affected, return a standard 404 as device wasn't found.
    if( $result < 1 ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    // If here then $result will store the number of records affected (hopefully 1)
    $response = [
      'message' => $app->trans('%name% was successfully %action%',
         [ '%action%' => 'deleted', '%name%' => $model::NICE_NAME_SINGLE ])
    ];
    return $app->json( $response, 200 );
  } // public function deleteOneSoft( Application $app, $id )



  /**
  * Updates an item in the database.
  */
  protected function updateOneItem( Application $app, Request $request, $id, array $update_fields=[], $force_account=false ) {

    $class = get_called_class();
    $model = $class::MODEL;

    /**
    * If model is 'account', disable access to master keys and authorised accounts.
    */
    if( $model == 'Medistudents\Models\Account' &&
        $id != $app['authorised.account'] &&
        API_ACL_MASTER != $app['authorised.access_level'] ) {

      $http_code = 403; // Forbidden
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    $original = $model::getByPKey( $app, $id );

    // If original was not found, return a 404.
    if( !$original ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    $item = new $model( $app );


    foreach( $item::SCHEMA as $f=>$v ) {
      $item->$f = $original->$f;
    }


    $updated_count = 0;
    $updated_uniques = [];

    foreach( $update_fields as $field ) {

      if( $request->get($field, API_MODEL_VALUE_NOT_UPDATED) !== API_MODEL_VALUE_NOT_UPDATED ) {

        $val = $request->get( $field );
        $val = DataFields::toResponse( $val, $model::SCHEMA[$field][1] );

        $item->$field = $val;

        if( $item->$field != $original->$field ) {
          $updated_count++;

          if( !empty($model::SCHEMA[$field][4]) ) {
            $updated_uniques[] = $field;
          }
        }
      }

      else {
        $item->$field = $original->$field;
      }
    }

    if( !$force_account )
      $item->account = $app['authorised.account'];

    $headers = [];
    $action = $app->trans('update');
    $action_ing = $app->trans('updating');
    $action_on = $app->trans( $model::NICE_NAME_SINGLE );


    // Run constraint validation against data object.

    $failed_validation_checks = false;
    $validation_response = null;


    if( $updated_count < 1 ) {

      $failed_validation_checks = true;

      $validation_response = [
        'type' => 'validation_failed',
        'code' => 'no_modified_data',
        'message' => $app->trans('No modified data provided when attempting to %action% %name%',
          ['%action%' => $action, '%name%' => $action_on] )
      ];
    }

    else {

      $validation_response = [
        'type' => 'validation_failed',
        'code' => 'errors_found',
        'message' => $app->trans('Validation errors found when %action% %name%',
          ['%action%' => $action_ing, '%name%' => $action_on] ),
        'issues' => array()
      ];

      // Check unique fields
      foreach( $updated_uniques as $field ) {
        if( !$item->isUnique( $field ) ) {

          $failed_validation_checks = true;

          $validation_response['issues'][] = [
            'param' => $field,
            'code' => 'not_unique',
            'message' => $app->trans( 'validators.field-not-unique-x', ['%name%'=>$field] )
          ];
        }
      }

      // Check validation constraints on data types
      $validation_errors = $app['validator']->validate( $item );

      if( count( $validation_errors ) > 0 ) {

        $failed_validation_checks = true;

        foreach ($validation_errors as $err) {

          $contraint = $err->getConstraint();

          $validation_response['issues'][] = [
            'param' => $err->getPropertyPath(),
            'code' => $contraint->payload['type'],
            'message' => $app->trans( $err->getMessage(), $contraint->payload[ $err->getMessage() ] )
          ];
        }
      } // if validation failed

    }


    // Validation failed.
    if( $failed_validation_checks ) {
      return $app->json( $validation_response, 422 );
    }

    // Validation passed so attempt an update.
    try {
      if( $item->dbUpdate() ) {

        // Reload the item.
        $item = $model::getByPKey( $app, $item->{$model::P_KEY} );

        // Retrieve embedded objects.
        $embed_requests = $app['request_stack']->getCurrentRequest()->get('embed');

        if( method_exists( $item, 'getEmbedsByPKey' ) && !empty($embed_requests) && is_array($embed_requests) ) {

          foreach( $embed_requests as $k => $embed_fld ) {

            if( property_exists($model, $embed_fld) ) {

              $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

              if( !empty($embed_item) ) {
                $item->$embed_fld = $embed_item->toResponse();
              }
              else {
                $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->$embed_fld );
              }
            }

            elseif( isset( $item->metadata->$embed_fld ) ) {

              $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->metadata->$embed_fld );

              if( !empty($embed_item) ) {
                $item->metadata->$embed_fld = $embed_item->toResponse();
              }
              else {
                $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->metadata->$embed_fld );
              }
            }
          }
        }

        // Add Location header to point to the newly created resource.
        $link_format = $app['app.url'] . $class::MOUNT_POINT . '/%i/';
        $headers['Location'] = sprintf( $link_format, $item->{$model::P_KEY} );

        return $app->json( $item->toResponse(), 200, $headers );
      }
    }
    catch( \Exception $e ) {
      if( $e->getCode() == 422 ) { // validation errors

        $response = [
          'type' => 'validation_failed',
          'code' => 'errors_found',
          'message' => $app->trans('Validation errors found when %action% %name%',
            ['%action%' => $action_ing, '%name%' => $action_on] ),
          'issues' => $item->getErrors()
        ];

        return $app->json( $response, 422 );
      }
    }

    // At this point, non-update is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => $action, '%name%' => $action_on]), 500);
  } // public function updateOneItem( Application $app, Request $request, $id, $non_update_fields=[], $update_fields=[] )



  /**
  * Disables an item from the database by setting is_disabled flag to true.
  */
  public function disableOneItem( Application $app, $id ) {

    $class = get_called_class();
    $model = $class::MODEL;

    /**
    * If model is 'account', disable access to master keys and authorised accounts.
    */
    if( $model == 'Medistudents\Models\Account' &&
        $id != $app['authorised.account'] &&
        API_ACL_MASTER != $app['authorised.access_level'] ) {

      $http_code = 403; // Forbidden
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    $table = $model::TABLE;

    $has_account = $model::HAS_ACCOUNT;
    $has_disable = $model::HAS_DISABLE;


    if( !$has_disable ) {
      $validation_response = [
        "type" => "invalid",
        "message" => $app->trans( 'validators.disable-unavailable' )
      ];
      return $app->json( $validation_response, 422 );
    }


    // Get the item.
    $item = $model::getByPKey( $app, $id );

    if( !$item ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    $original = clone $item;

    // Toggle the disable setting.
    if( $item->is_disabled === false ) {
      $item->is_disabled = true;
    }
    else {
      $item->is_disabled = false;
    }


    if( $item->dbUpdate() ) {

      $action = $item->is_disabled ? 'disabled/hidden' : 'enabled';

      $response = [
        'message' => $app->trans('%name% was successfully %action%',
           [ '%action%' => $action, '%name%' => $model::NICE_NAME_SINGLE ])
      ];
      return $app->json( $response, 200 );
    }

    // At this point, non-update is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => 'update', '%name%' => $model::NICE_NAME_SINGLE]), 500);

  } // public function disableOneItem( Application $app, $id )




  /**
  * Adds a new item to the database.
  */
  protected function addOneItem( Application $app, Request $request, $item, $force_account=false ) {

    $class = get_called_class();
    $model = get_class( $item );

    /**
    * If model is 'account', disable access to master keys and authorised accounts.
    */
    if( $model == 'Medistudents\Models\Account' &&
        API_ACL_MASTER != $app['authorised.access_level'] ) {

      $http_code = 403; // Forbidden
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    if( !$force_account )
      $item->account = $app['authorised.account'];

    $headers = [];
    $action = $app->trans('add');
    $action_ing = $app->trans('adding');
    $action_on = $app->trans( $model::NICE_NAME_SINGLE );


    // Run constraint validation against data object.
    $failed_validation_checks = false;
    $validation_response = null;


    $validation_response = [
      'type' => 'validation_failed',
      'code' => 'errors_found',
      'message' => $app->trans('Validation errors found when %action% %name%',
        ['%action%' => $action_ing, '%name%' => $action_on] ),
      'issues' => array()
    ];

    // Check unique fields
    foreach( $model::SCHEMA as $field=>$attr ) {
      if( !empty($attr[4]) && !$item->isUnique( $field ) ) {

        $failed_validation_checks = true;

        $validation_response['issues'][] = [
          'param' => $field,
          'code' => 'not_unique',
          'message' => $app->trans( 'validators.field-not-unique-x', ['%name%'=>$field] )
        ];
      }
    }

    // Check validation constraints on data types
    $validation_errors = $app['validator']->validate( $item );

    if( count( $validation_errors ) > 0 ) {

      $failed_validation_checks = true;

      foreach ($validation_errors as $err) {

        $contraint = $err->getConstraint();

        $validation_response['issues'][] = [
          'param' => $err->getPropertyPath(),
          'code' => $contraint->payload['type'],
          'message' => $app->trans( $err->getMessage(), $contraint->payload[ $err->getMessage() ] )
        ];
      }
    } // if validation failed


    // Validation failed.
    if( $failed_validation_checks ) {
      return $app->json( $validation_response, 422 );
    }


    // Validation passed so attempt an insert.
    try {
      if( $item->dbInsert() ) {

        // Reload the item.
        $item = $model::getByPKey( $app, $item->{$model::P_KEY} );

        // Add Location header to point to the newly created resource.
        $link_format = $app['app.url'] . $class::MOUNT_POINT . '/%s/';
        $headers['Location'] = sprintf( $link_format, $item->{$model::P_KEY} );

        return $app->json( $item->toResponse(), 201, $headers );
      }
    }
    catch( \Exception $e ) {
      if( $e->getCode() == 422 ) { // validation errors

        $response = [
          'type' => 'validation_failed',
          'code' => 'errors_found',
          'message' => $app->trans('Validation errors found when %action% %name%',
            ['%action%' => $action_ing, '%name%' => $action_on] ),
          'issues' => $item->getErrors()
        ];

        return $app->json( $response, 422 );
      }
    }

    // At this point, non-addition is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => $action, '%name%' => $action_on]), 500);

  } // public function addOneItem( Application $app, $item )



  /**
  * Retrieves the headers for pagination and linking.
  */
  protected function getPaginationHeaders( Application $app, int $per_page, int $page, $options=[] ) {

    // Set default options if not already set.
    if( !array_key_exists('format', $options) )
      $options['format'] = 'default';

    if( !array_key_exists('query_extra', $options) )
      $options['query_extra'] = null;

    $class = get_called_class();
    $model = isset( $options['model'] ) ? $options['model'] : $class::MODEL;

    $table = $model::TABLE;
    $p_key = $model::P_KEY;
    $has_soft_delete = $model::HAS_SOFT_DELETE;
    $has_account = $model::HAS_ACCOUNT;

    $offset = ($page - 1) * $per_page;

    $prepared_sql = '';
    $prepared_stmt_values = [];
    $prepared_stmt_types = [];

    $sql = "
      SELECT COUNT(*)
      FROM %s AS tbl
      WHERE 1=1
      ". ($has_account && $app['authorised.access_level'] !== API_ACL_MASTER ? "AND tbl.account_id = ?" : "" )."
      ". ($has_soft_delete ? "AND tbl.is_deleted = 'N'" : "" ) ."
      ". $options['query_extra'];

    $prepared_sql = sprintf( $sql, $table );

    if( $has_account && $app['authorised.access_level'] !== API_ACL_MASTER ) {
      $prepared_stmt_values[] = $app['authorised.account'];
      $prepared_stmt_types[] = \PDO::PARAM_INT;
    }

    $stmt = $app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

    $count_all = (int)$stmt->fetchColumn();

    $headers['X-Page'] = $page;
    $headers['X-Per-Page'] = $per_page;
    $headers['X-Total-Count'] = $count_all;

    //
    // Get the pagination links.
    $page_last = ceil( $count_all / $per_page );

    $link_format = '<' . $app['app.url'] . $class::MOUNT_POINT . '/?per_page=%s&page=%s>; rel="%s"';
    $links = [];

    if( $page > 1 ) {
      $links[] = sprintf( $link_format, $per_page, 1, 'first' );
      $links[] = sprintf( $link_format, $per_page, $page-1, 'prev' );
    }

    if( $page < $page_last ) {
      $links[] = sprintf( $link_format, $per_page, $page+1, 'next' );
      $links[] = sprintf( $link_format, $per_page, $page_last, 'last' );
    }

    if( count($links) > 0 )
      $headers['Link'] = implode( ',', $links );

    return $headers;
  }
} // class
