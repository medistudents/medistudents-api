<?php
namespace Medistudents\Controllers;

use Silex\Application,
    Silex\Api\ControllerProviderInterface,
    Symfony\Component\HttpFoundation\Request AS Request,
    Symfony\Component\HttpFoundation\Response AS Response,
    Symfony\Component\HttpFoundation\JsonResponse,
    Medistudents\Helpers\HttpErrorResponses AS HttpErrorResponses,
    Medistudents\Helpers\DataFields AS DataFields,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\Subscription AS Subscription;

class SubscriptionsController extends AbstractDataController {

  const MODEL  = 'Medistudents\Models\Subscription';
  const MOUNT_POINT = '/subscriptions';


  /**
  * Define routes.
  */
  public function connect( Application $app ) {

    $controllers = $app['controllers_factory'];

    // Return item for an account.
    $controllers->get( '/', __CLASS__.'::getMany' );

    // Retrieves subscriptions items belonging to an account.
    $controllers->get( '/account/{id}/', __CLASS__.'::getManyByAccount' );

    // Retrieves subscriptions items belonging to an account.
    $controllers->get( '/account/{id}/not-cancelled/', __CLASS__.'::getManyByAccountNotCancelled' );


    // Retrieves a single item for an account.
    $controllers->get( '/{id}/', __CLASS__.'::getOne' );


    // Adds a new item to an account.
    $controllers->post( '/', __CLASS__.'::addOne' );

    // Updates a item on an account.
    $controllers->patch( '/{id}/', __CLASS__.'::updateOne' );


    // Sets a subscription to Cancelled status.
    $controllers->delete( '/{id}/', __CLASS__.'::setToCancelled' );

    // Deletes (hard) a single item for an account.
    $controllers->delete( '/{id}/hard/', __CLASS__.'::deleteOneHard' );





    // Sets a subscription to Active status.
    $controllers->patch( '/{id}/active/', __CLASS__.'::setToActive' );

    // Sets a subscription to Trial status.
    $controllers->patch( '/{id}/trial/', __CLASS__.'::setToTrial' );

    // Sets a subscription to Unpaid status.
    $controllers->patch( '/{id}/unpaid/', __CLASS__.'::setToUnpaid' );



    // Sets a token to a subscription.
    $controllers->patch( '/{id}/token/{token}/', __CLASS__.'::setTokenValue' );

    // Sets status to active and add token to a subscription.
    $controllers->patch( '/{id}/active/token/{token}/', __CLASS__.'::setStatusToActiveAndTokenValue' );

    // Sets status to active and add token to a subscription.
    $controllers->patch( '/{id}/trial/token/{token}/', __CLASS__.'::setStatusToTrialAndTokenValue' );

    // Sets status to active and add token to a subscription.
    $controllers->patch( '/{id}/unpaid/token/{token}/', __CLASS__.'::setStatusToUnpaidAndTokenValue' );



    // Deletes a token from an subscription.
    $controllers->delete( '/{id}/token/', __CLASS__.'::setTokenValue' );


    return $controllers;
  }


  /**
  * Retrieves the subscriptions stored in an account.
  */
  public function getManyByAccount( Application $app, Request $request, $id ) {

    $fld_account = Subscription::SCHEMA['account'][0];

    $query_extra = sprintf("AND %s = %d", $fld_account, $id);
    return $this->getMany( $app, $request, ['query_extra'=>$query_extra] );
  }

  /**
  * Retrieves the subscriptions stored in an account which are not cancelled.
  */
  public function getManyByAccountNotCancelled( Application $app, Request $request, $id ) {

    $fld_account = Subscription::SCHEMA['account'][0];
    $fld_status = Subscription::SCHEMA['status'][0];

    $query_extra = sprintf("
      AND %s = %d
      AND %s != '%s'
    ",
      $fld_account, $id,
      $fld_status, Subscription::STATUS_CANCELLED
    );

    return $this->getMany( $app, $request, ['query_extra'=>$query_extra] );
  }



  /**
  * Adds an item to the database.
  */
  public function addOne( Application $app, Request $request ) {

    $model = $this::MODEL;
    $item = new $model( $app );

    //- START Set values from API call ----------------------------------------

    $item->account           = $request->get('account');
    $item->plan              = $request->get('plan');
    $item->status            = $request->get('status');
    $item->tax_percent       = $request->get('tax_percent');
    $item->discount_percent  = $request->get('status');
    $item->token             = $request->get('token');
    $item->metadata          = DataFields::toResponse( $request->get('metadata'), 'keyvalue' );

    $item->tax_percent = !empty($request->get('tax_percent'))
      ? $request->get('tax_percent') : 0;

    $item->discount_percent = !empty($request->get('discount_percent'))
      ? $request->get('discount_percent') : 0;

    //- END Set values from API call ------------------------------------------


    return $this->addOneItem( $app, $request, $item, true );
  }

  /**
  * Updates an item in the database.
  */
  public function updateOne( Application $app, Request $request, $id ) {

    $model = $this::MODEL;
    return $this->updateOneItem( $app, $request, $id, $model::UPDATABLE_FIELDS, true );
  }


  /**
  * Sets a subscription to an Active state.
  */
  public function setToActive( Application $app, $id ) {

    $class = get_called_class();
    $model = $class::MODEL;

    return $this->setTo( $app, $id, $model::STATUS_ACTIVE );
  }


  /**
  * Sets a subscription to an Trial state.
  */
  public function setToTrial( Application $app, $id ) {

    $class = get_called_class();
    $model = $class::MODEL;

    return $this->setTo( $app, $id, $model::STATUS_TRIAL );
  }

  /**
  * Sets a subscription to an Unpaid state.
  */
  public function setToUnpaid( Application $app, $id ) {

    $class = get_called_class();
    $model = $class::MODEL;

    return $this->setTo( $app, $id, $model::STATUS_UNPAID );
  }

  /**
  * Sets a subscription to an Cancelled state.
  */
  public function setToCancelled( Application $app, $id ) {

    $class = get_called_class();
    $model = $class::MODEL;

    return $this->setTo( $app, $id, $model::STATUS_CANCELLED );
  }

  /**
  * Sets a subscription to a certain state.
  */
  private function setTo( Application $app, $id, $status ) {

      $class = get_called_class();
      $model = $class::MODEL;
      $table = $model::TABLE;

      $headers = [];

      // Only process if valid status.
      if( $model::isValidStatus($status) ) {

        // Get the item.
        $item = $model::getByPKey( $app, $id );

        if( !$item ) {
          $http_code = 404;
          $error = HttpErrorResponses::getErr( $http_code );
          return $app->json( $error, $http_code );
        }

        $item->status = $status;


        if( $item->dbUpdate() ) {

          // Retrieve embedded objects.
          $embed_requests = $app['request_stack']->getCurrentRequest()->get('embed');

          if( method_exists( $item, 'getEmbedsByPKey' ) && !empty($embed_requests) && is_array($embed_requests) ) {

            foreach( $embed_requests as $k => $embed_fld ) {

              if( property_exists($model, $embed_fld) ) {

                $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

                if( !empty($embed_item) ) {
                  $item->$embed_fld = $embed_item->toResponse();
                }
                else {
                  $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->$embed_fld );
                }
              }

              elseif( isset( $item->metadata->$embed_fld ) ) {

                $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->metadata->$embed_fld );

                if( !empty($embed_item) ) {
                  $item->metadata->$embed_fld = $embed_item->toResponse();
                }
                else {
                  $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->metadata->$embed_fld );
                }
              }
            }
          }

          return $app->json( $item->toResponse(), 200, $headers );
        }
      }

      // At this point, non-update is a server error
      return new Response( $app->trans('Application failed to %action% %name%',
        ['%action%' => 'update', '%name%' => $model::NICE_NAME_SINGLE]), 500);
  }



  /**
  * Sets the token value.
  */
  public function setTokenValue( Application $app, $id, string $token=null ) {

    $class = get_called_class();
    $model = $class::MODEL;
    $table = $model::TABLE;

    $headers = [];


    // Get the item.
    $item = $model::getByPKey( $app, $id );

    if( !$item ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    $item->token = $token;


    if( $item->dbUpdate() ) {

      // Retrieve embedded objects.
      $embed_requests = $app['request_stack']->getCurrentRequest()->get('embed');

      if( method_exists( $item, 'getEmbedsByPKey' ) && !empty($embed_requests) && is_array($embed_requests) ) {

        foreach( $embed_requests as $k => $embed_fld ) {

          if( property_exists($model, $embed_fld) ) {

            $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

            if( !empty($embed_item) ) {
              $item->$embed_fld = $embed_item->toResponse();
            }
            else {
              $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->$embed_fld );
            }
          }

          elseif( isset( $item->metadata->$embed_fld ) ) {

            $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->metadata->$embed_fld );

            if( !empty($embed_item) ) {
              $item->metadata->$embed_fld = $embed_item->toResponse();
            }
            else {
              $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->metadata->$embed_fld );
            }
          }
        }
      }

      return $app->json( $item->toResponse(), 200, $headers );
    }

    // At this point, non-update is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => 'update', '%name%' => $model::NICE_NAME_SINGLE]), 500);

  } // public function setTokenValue( Application $app, $id, string $token=null )



  /**
  * Sets the status of a subscription to Active and adds a token value.
  */
  public function setStatusToActiveAndTokenValue( Application $app, $id, string $token=null ) {

    $class = get_called_class();
    $model = $class::MODEL;

    return $this->setStatusAndTokenValue( $app, $id, $model::STATUS_ACTIVE, $token );
  }

  /**
  * Sets the status of a subscription to Trial and adds a token value.
  */
  public function setStatusToTrialAndTokenValue( Application $app, $id, string $token=null ) {

    $class = get_called_class();
    $model = $class::MODEL;

    return $this->setStatusAndTokenValue( $app, $id, $model::STATUS_TRIAL, $token );
  }

  /**
  * Sets the status of a subscription to Unpaid and adds a token value.
  */
  public function setStatusToUnpaidAndTokenValue( Application $app, $id, string $token=null ) {

    $class = get_called_class();
    $model = $class::MODEL;

    return $this->setStatusAndTokenValue( $app, $id, $model::STATUS_UNPAID, $token );
  }


  /**
  * Sets the token value and status.
  */
  public function setStatusAndTokenValue( Application $app, $id, string $status, string $token=null ) {

    $class = get_called_class();
    $model = $class::MODEL;
    $table = $model::TABLE;

    $headers = [];


    // Only process if valid status.
    if( $model::isValidStatus($status) ) {

      // Get the item.
      $item = $model::getByPKey( $app, $id );

      if( !$item ) {
        $http_code = 404;
        $error = HttpErrorResponses::getErr( $http_code );
        return $app->json( $error, $http_code );
      }

      $item->status = $status;
      $item->token = $token;


      if( $item->dbUpdate() ) {

        // Retrieve embedded objects.
        $embed_requests = $app['request_stack']->getCurrentRequest()->get('embed');

        if( method_exists( $item, 'getEmbedsByPKey' ) && !empty($embed_requests) && is_array($embed_requests) ) {

          foreach( $embed_requests as $k => $embed_fld ) {

            if( property_exists($model, $embed_fld) ) {

              $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

              if( !empty($embed_item) ) {
                $item->$embed_fld = $embed_item->toResponse();
              }
              else {
                $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->$embed_fld );
              }
            }

            elseif( isset( $item->metadata->$embed_fld ) ) {

              $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->metadata->$embed_fld );

              if( !empty($embed_item) ) {
                $item->metadata->$embed_fld = $embed_item->toResponse();
              }
              else {
                $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->metadata->$embed_fld );
              }
            }
          }
        }

        return $app->json( $item->toResponse(), 200, $headers );
      }
    }

    // At this point, non-update is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => 'update', '%name%' => $model::NICE_NAME_SINGLE]), 500);
  }

} // class
