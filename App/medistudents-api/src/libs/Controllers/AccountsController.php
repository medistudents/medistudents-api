<?php
namespace Medistudents\Controllers;

use Silex\Application,
    Silex\Api\ControllerProviderInterface,
    Symfony\Component\HttpFoundation\Request AS Request,
    Symfony\Component\HttpFoundation\Response AS Response,
    Symfony\Component\HttpFoundation\JsonResponse,
    Medistudents\Helpers\HttpErrorResponses AS HttpErrorResponses,
    Medistudents\Helpers\DataFields AS DataFields,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\Account AS Account,
    Medistudents\Models\AccountUser AS AccountUser,
    Medistudents\Models\Timezone AS Timezone,
    Medistudents\Controllers\SubscriptionsController AS SubscriptionsController,
    Medistudents\Controllers\UsersController AS UsersController;

class AccountsController extends AbstractDataController {

  const MODEL  = 'Medistudents\Models\Account';
  const MOUNT_POINT = '/accounts';


  /**
  * Define routes.
  */
  public function connect( Application $app ) {

    $controllers = $app['controllers_factory'];

    // Return item for an account.
    $controllers->get( '/', __CLASS__.'::getMany' );

    // Retrieves a single item for an account.
    $controllers->get( '/{id}/', __CLASS__.'::getOne' );


    // Retrieves Subscriptions the Account has.
    $controllers->get( '/{id}/subscriptions/', __CLASS__.'::getSubscriptions' );

    // Retrieves Subscriptions the Account has (not cancelled)
    $controllers->get( '/{id}/subscriptions/not-cancelled/', __CLASS__.'::getSubscriptionsNotCancelled' );


    // Retrieves Users the Account has.
    $controllers->get( '/{id}/users/', __CLASS__.'::getUsers' );


    // Adds a new item to an account.
    $controllers->post( '/', __CLASS__.'::addOne' );

    // Updates a item on an account.
    $controllers->patch( '/{id}/', __CLASS__.'::updateOne' );


    // Deletes (softly) a single item for an account.
    $controllers->delete( '/{id}/', __CLASS__.'::deleteOneSoft' );

    // Deletes (hard) a single item for an account.
    $controllers->delete( '/{id}/hard/', __CLASS__.'::deleteOneHard' );


    // Toggles a single item's disabled status.
    $controllers->patch( '/{id}/disable/', __CLASS__.'::disableOneItem' );



    // Sets a token to an account.
    $controllers->patch( '/{id}/token/{token}/', __CLASS__.'::setTokenValue' );

    // Deletes a token from an account.
    $controllers->delete( '/{id}/token/', __CLASS__.'::setTokenValue' );



    // Adds a user to the account.
    $controllers->post( '/{id}/user/{user}/permission/{permission}/', __CLASS__.'::addUserToAccount' );


    // Removes a user from the account.
    $controllers->delete( '/{id}/user/{user}/', __CLASS__.'::deleteUserFromAccount' );



    return $controllers;
  }


  /**
  * Updates an item in the database.
  */
  public function updateOne( Application $app, Request $request, $id ) {

    $model = $this::MODEL;
    return $this->updateOneItem( $app, $request, $id, $model::UPDATABLE_FIELDS );
  }


  /**
  * Adds an item to the database.
  */
  public function addOne( Application $app, Request $request ) {

    $model = $this::MODEL;
    $item = new $model( $app );

    //- START Set values from API call ----------------------------------------

    $item->is_disabled = !empty( $request->get('is_disabled') )
      ? DataFields::toResponse( $request->get('is_disabled'), 'boolean' )
      : false;

    $item->is_deleted = !empty( $request->get('is_deleted') )
      ? DataFields::toResponse( $request->get('is_deleted'), 'boolean' )
      : false;

    $item->name           = $request->get('name');
    $item->recovery_email = $request->get('recovery_email');
    $item->country        = $request->get('country');
    $item->metadata       = DataFields::toResponse( $request->get('metadata'), 'keyvalue' );


    // Timezone if identifier passed
    if( false === filter_var( $request->get('timezone'), FILTER_VALIDATE_INT, [
      'options' => [ 'default' => false ] ])) {

      $tz = Timezone::getByIdentifier( $app, [$request->get('timezone')], 'timezone' );
      $request->query->set( 'timezone', ($tz ? $tz->id : null) );
    }
    $item->timezone = !empty($request->get('timezone'))
      ? $request->get('timezone')
      : $app['app.user.default_timezone'];


    // Language if null passed
    $request->query->set( 'language', ( !empty($request->get('language'))
      ? $request->get('language')
      : $app['app.user.default_language']) );

    $item->language = $request->get('language');


    //- END Set values from API call ------------------------------------------


    return $this->addOneItem( $app, $request, $item );
  }


  /**
  * Adds a user to an account.
  */
  public function addUserToAccount( Application $app, int $id, int $user, int $permission ) {

    $class = get_called_class();
    $model = 'Medistudents\Models\AccountUser';

    $item = new $model( $app );

    $item->account = $id;
    $item->user = $user;
    $item->permission = $permission;


    $headers = [];
    $action = $app->trans('add');
    $action_ing = $app->trans('adding');
    $action_on = $app->trans( $model::NICE_NAME_SINGLE );


    // Run constraint validation against data object.
    $failed_validation_checks = false;
    $validation_response = null;


    $validation_response = [
      'type' => 'validation_failed',
      'code' => 'errors_found',
      'message' => $app->trans('Validation errors found when %action% %name%',
        ['%action%' => $action_ing, '%name%' => $action_on] ),
      'issues' => array()
    ];

    // Check unique fields
    foreach( $model::SCHEMA as $field=>$attr ) {
      if( !empty($attr[4]) && !$item->isUnique( $field ) ) {

        $failed_validation_checks = true;

        $validation_response['issues'][] = [
          'param' => $field,
          'code' => 'not_unique',
          'message' => $app->trans( 'validators.field-not-unique-x', ['%name%'=>$field] )
        ];
      }
    }

    // Check validation constraints on data types
    $validation_errors = $app['validator']->validate( $item );

    if( count( $validation_errors ) > 0 ) {

      $failed_validation_checks = true;

      foreach ($validation_errors as $err) {

        $contraint = $err->getConstraint();

        $validation_response['issues'][] = [
          'param' => $err->getPropertyPath(),
          'code' => $contraint->payload['type'],
          'message' => $app->trans( $err->getMessage(), $contraint->payload[ $err->getMessage() ] )
        ];
      }
    } // if validation failed


    // Validation failed.
    if( $failed_validation_checks ) {
      return $app->json( $validation_response, 422 );
    }


    // Validation passed so attempt an insert.
    try {
      if( $item->dbInsert() ) {

        // Reload the item.
        $item = $model::getByPKey( $app, $item->{$model::P_KEY} );

        // Add Location header to point to the newly created resource.
        $link_format = $app['app.url'] . $class::MOUNT_POINT . '/%s/';
        $headers['Location'] = sprintf( $link_format, $item->{$model::P_KEY} );


        $response = [
          'type' => 'success',
          'message' => $app->trans( 'validators.success-relation', [
            '%name_a%' => 'user',
            '%name_b%' => 'account'
          ])
        ];
        return $app->json( $response, 201 );
      }
    }
    catch( \Exception $e ) {
      if( $e->getCode() == 422 ) { // validation errors

        $response = [
          'type' => 'validation_failed',
          'code' => 'errors_found',
          'message' => $app->trans('Validation errors found when %action% %name%',
            ['%action%' => $action_ing, '%name%' => $action_on] ),
          'issues' => $item->getErrors()
        ];

        return $app->json( $response, 422 );
      }
    }

    // At this point, non-addition is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => $action, '%name%' => $action_on]), 500);
  }


  /**
  * Deletes a user from an account.
  */
  public function deleteUserFromAccount( Application $app, int $id, int $user ) {
    // TODO: Write public function deleteUserFromAccount( Application $app, int $id, int $user )
  }



  /**
  * Returns the subscriptions the account has.
  */
  public function getSubscriptions( Application $app, Request $request, $id ) {

    $subscriptions_controller = new SubscriptionsController( $app );

    $accounts = $subscriptions_controller->getManyByAccount( $app, $request, $id );

    return $accounts;

  } // public function getSubscriptions( Application $app, Request $request, $id )


  /**
  * Returns the not-cancelled subscriptions the account has.
  */
  public function getSubscriptionsNotCancelled( Application $app, Request $request, $id ) {

    $subscriptions_controller = new SubscriptionsController( $app );

    $accounts = $subscriptions_controller->getManyByAccountNotCancelled( $app, $request, $id );

    return $accounts;

  } // public function getSubscriptionsNotCancelled( Application $app, Request $request, $id )




  /**
  * Returns the users the account has.
  */
  public function getUsers( Application $app, Request $request, $id ) {

    $users_controller = new UsersController( $app );

    $accounts = $users_controller->getManyByAccount( $app, $request, $id );

    return $accounts;

  } // public function getUsers( Application $app, Request $request, $id )



  /**
  * Sets the token value.
  */
  public function setTokenValue( Application $app, $id, string $token=null ) {

    $class = get_called_class();
    $model = $class::MODEL;
    $table = $model::TABLE;

    $headers = [];


    // Get the item.
    $item = $model::getByPKey( $app, $id );

    if( !$item ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    $item->token = $token;


    if( $item->dbUpdate() ) {

      // Retrieve embedded objects.
      $embed_requests = $app['request_stack']->getCurrentRequest()->get('embed');

      if( method_exists( $item, 'getEmbedsByPKey' ) && !empty($embed_requests) && is_array($embed_requests) ) {

        foreach( $embed_requests as $k => $embed_fld ) {

          if( property_exists($model, $embed_fld) ) {

            $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

            if( !empty($embed_item) ) {
              $item->$embed_fld = $embed_item->toResponse();
            }
            else {
              $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->$embed_fld );
            }
          }

          elseif( isset( $item->metadata->$embed_fld ) ) {

            $embed_item = $model::getEmbedsByPKey( $app, $embed_fld, $item->metadata->$embed_fld );

            if( !empty($embed_item) ) {
              $item->metadata->$embed_fld = $embed_item->toResponse();
            }
            else {
              $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->metadata->$embed_fld );
            }
          }
        }
      }

      return $app->json( $item->toResponse(), 200, $headers );
    }

    // At this point, non-update is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => 'update', '%name%' => $model::NICE_NAME_SINGLE]), 500);

  } // public function setTokenValue( Application $app, $id, string $token=null )


} // class
