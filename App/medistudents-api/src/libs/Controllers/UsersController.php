<?php
namespace Medistudents\Controllers;

use Silex\Application,
    Silex\Api\ControllerProviderInterface,
    Symfony\Component\HttpFoundation\Request AS Request,
    Symfony\Component\HttpFoundation\Response AS Response,
    Symfony\Component\HttpFoundation\JsonResponse,
    Medistudents\Helpers\HttpErrorResponses AS HttpErrorResponses,
    Medistudents\Helpers\DataFields AS DataFields,
    Medistudents\controllers\AccountsController AS AccountsController,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\User AS User,
    Medistudents\Models\AccountUser AS AccountUser,
    Medistudents\Models\Timezone AS Timezone;

class UsersController extends AbstractDataController {

  const MODEL  = 'Medistudents\Models\User';
  const MOUNT_POINT = '/users';


  /**
  * Define routes.
  */
  public function connect( Application $app ) {

    $controllers = $app['controllers_factory'];

    // Return item for an account.
    $controllers->get( '/', __CLASS__.'::getMany' );

    // Retrieves users belonging to an account.
    $controllers->get( '/account/{id}/', __CLASS__.'::getManyByAccount' );

    // Retrieves a single item for an account.
    $controllers->get( '/{id}/', __CLASS__.'::getOne' );


    // Retrieves accounts user belongs to.
    $controllers->get( '/{id}/accounts/', __CLASS__.'::getAccounts' );


    // Adds a new item to an account.
    $controllers->post( '/', __CLASS__.'::addOne' );

    // Updates a item on an account.
    $controllers->patch( '/{id}/', __CLASS__.'::updateOne' );



    // Deletes (softly) a single item for an account.
    $controllers->delete( '/{id}/', __CLASS__.'::deleteOneSoft' );

    // Deletes (hard) a single item for an account.
    $controllers->delete( '/{id}/hard/', __CLASS__.'::deleteOneHard' );



    return $controllers;
  }


  /**
  * Returns the accounts that the user belongs to.
  */
  public function getAccounts( Application $app, Request $request, $id ) {

    //
    // Retrieve the ID's of Accounts that a User is a member of.
    // IDEA [DB Optimisation] Can remove a read query by creating objects directly from on JOIN query.
    //

    $account_user_model = new AccountUser($app);

    $prepared_sql = '';
    $prepared_stmt_values = [];
    $prepared_stmt_types = [];


    $sql = "
      SELECT DISTINCT(tbl.%s) AS account_id
      FROM %s AS tbl
      WHERE tbl.%s = ?
    ";

    $prepared_sql = sprintf(
      $sql,
      $account_user_model::SCHEMA['account'][0],
      $account_user_model::TABLE,
      $account_user_model::SCHEMA['user'][0]
    );

    $prepared_stmt_values[] = $id;
    $prepared_stmt_types[] = \PDO::PARAM_INT;

    $stmt = $app['db']->executeQuery(
      $prepared_sql, $prepared_stmt_values, $prepared_stmt_types );

    $result = $stmt->fetchAll();

    // No accounts, return a 404.
    if( empty( $result ) ) {

      if( $options['format'] == 'objects' )
        return false;

      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    $accounts = [];

    foreach( $result as $r ) {
      $accounts[] = $r['account_id'];
    }


    //
    // Use the AccountsController class to retrieve those accounts and return.
    //
    $query_extra = $query_extra = sprintf("AND id IN ( %s )", implode( ',', $accounts ) );

    return (new AccountsController)->getMany( $app, $request, ['query_extra'=>$query_extra] );

  } // public function getAccounts( Application $app, Request $request, $id )


  /**
  * Retrieves the users stored in an account.
  */
  public function getManyByAccount( Application $app, Request $request, $id ) {

    $model = $this::MODEL;
    $item = new $model( $app );

    $account_user_model = new AccountUser($app);

    $joins = sprintf( "
      INNER JOIN %s AS tbl2
        ON tbl2.%s = tbl.%s
        AND tbl2.%s = %d
    ",
      $account_user_model::TABLE,
      $account_user_model::SCHEMA['user'][0],
      $item::SCHEMA['id'][0],
      $account_user_model::SCHEMA['account'][0],
      $id
    );

    $query_extra = sprintf( 'GROUP BY %s.%s', 'tbl', $item::SCHEMA['id'][0] );

    return $this->getMany( $app, $request, ['joins'=>$joins, 'query_extra'=>$query_extra] );
  }


  /**
  * Updates an item in the database.
  */
  public function updateOne( Application $app, Request $request, $id ) {

    $updating_user = null;
    $data_validation_errors = [];


    $new_password_hash = null; // if needed


    // If updating the email, need to also supply password for re-hashing
    if( !empty($request->get('email')) && empty($request->get('password')) ) {
      $data_validation_errors[] = [
        'param' => 'email',
        'code' => 'password_required',
        'message' => $app->trans( 'validators.password-required-to-update-email-x', ['%name%'=>'email'] )
      ];
    }

    // If only updating password, need to retrieve email from DB for hashing.
    else if( empty($request->get('email')) && !empty($request->get('password')) ) {

      // If updating the email or the password, need to retrieve the User
      $updating_user = User::getByPKey( $app, $id );

      if( !empty($updating_user) ) {

        $new_password_hash = Utilities::toPassword(
          $app['app.security.algo'],
          $request->get('password'),
          $updating_user->email,
          $app['app.security.salt'],
          $app['app.security.cost'] );
      }
    }

    // If updating both, can use new email as salt for hashing.
    else if( !empty($request->get('email')) && !empty($request->get('password')) ) {

      $request->query->set( 'email', strtolower($request->get('email')) ); // all emails should be lowercase

      $new_password_hash = Utilities::toPassword(
        $app['app.security.algo'],
        $request->get('password'),
        $request->get('email'),
        $app['app.security.salt'],
        $app['app.security.cost'] );
    }

    // Set the new password hash if it's been changed.
    if( !empty( $new_password_hash ) ) {
      $request->query->set( 'password', $new_password_hash );
    }



    // Timezone if identifier passed.
    if( $request->get('timezone')
      && false === filter_var( $request->get('timezone'), FILTER_VALIDATE_INT, [
      'options' => [ 'default' => false ] ])) {

      $tz = Timezone::getByIdentifier( $app, [$request->get('timezone')], 'timezone' );
      $request->query->set( 'timezone', ($tz ? $tz->id : -1) );
    }



    // Return a 422 if there are validation errors.
    if( !empty($data_validation_errors) ) {

      $validation_response = [
        'type' => 'validation_failed',
        'code' => 'errors_found',
        'message' => $app->trans('Validation errors found when %action% %name%',
          ['%action%' => 'updating', '%name%' => 'User'] ),
        'issues' => $data_validation_errors
      ];

      return $app->json( $validation_response, 422 );
    }

    $model = $this::MODEL;
    return $this->updateOneItem( $app, $request, $id, $model::UPDATABLE_FIELDS );
  }


  /**
  * Adds an item to the database.
  */
  public function addOne( Application $app, Request $request ) {

    $model = $this::MODEL;
    $item = new $model( $app );


    //- START Set values from API call ----------------------------------------

    $item->email      = strtolower( $request->get('email') ); // all emails must be lowercase
    $item->name       = $request->get('name');
    $item->metadata   = DataFields::toResponse( $request->get('metadata'), 'keyvalue' );

    // Password hashing
    $item->password = !empty( $request->get('password') )
      ? Utilities::toPassword(
          $app['app.security.algo'],
          $request->get('password'),
          $item->email,
          $app['app.security.salt'],
          $app['app.security.cost'] )
      : null;

    // Timezone if identifier passed
    if( false === filter_var( $request->get('timezone'), FILTER_VALIDATE_INT, [
      'options' => [ 'default' => false ] ])) {

      $tz = Timezone::getByIdentifier( $app, [$request->get('timezone')], 'timezone' );
      $request->query->set( 'timezone', ($tz ? $tz->id : null) );
    }
    $item->timezone = !empty($request->get('timezone'))
      ? $request->get('timezone')
      : $app['app.user.default_timezone'];

    // Language if null passed
    $request->query->set( 'language', ( !empty($request->get('language'))
      ? $request->get('language')
      : $app['app.user.default_language']) );

    $item->language = $request->get('language');


    //- END Set values from API call ------------------------------------------


    return $this->addOneItem( $app, $request, $item );
  } // public function addOne()

} // class
