<?php
namespace Medistudents\Controllers;

use Silex\Application,
    Silex\Api\ControllerProviderInterface,
    Symfony\Component\HttpFoundation\Request AS Request,
    Symfony\Component\HttpFoundation\Response AS Response,
    Symfony\Component\HttpFoundation\JsonResponse,
    Medistudents\Helpers\HttpErrorResponses AS HttpErrorResponses,
    Medistudents\Helpers\Utilities AS Utilities,
    Medistudents\Models\User,
    Medistudents\Models\AccountUser,
    Medistudents\Models\Country,
    Medistudents\Models\Timezone,
    Medistudents\Models\Language;

class AuthenticateController extends AbstractDataController implements ControllerProviderInterface {

  const MOUNT_POINT = '/auth';


  /**
  * Define routes.
  */
  public function connect( Application $app ) {

    $controllers = $app['controllers_factory'];

    // Authenticates a user.
    $controllers->post( '/', __CLASS__.'::authenticateUser' );

    // Retrieves permissions of a user.
    $controllers->get( '/{user_id}/permissions/', __CLASS__.'::getUserPermissions' );


    // Initiates a password reset for a username.
    $controllers->patch( '/forgot-password/', __CLASS__.'::forgotPassword' );

    // Resets a password for a user after a forgot password request.
    $controllers->patch( '/reset-password/{reset_code}/', __CLASS__.'::resetPassword' );


    return $controllers;
  }


  /**
  * Authenticates that a user exists with the supplied username/password combination.
  * Will return the User if it exists.
  */
  public function authenticateUser( Application $app, Request $request ) {

    // Following used for logging.
    $provided_api_key = $_SERVER['PHP_AUTH_USER'];
    $obfuscated_api_key = substr( $provided_api_key, 0, 6 ) . '**************************';

    $headers = [];
    $options = [];

    $username = $request->get('username');
    $password = $request->get('password');

    // Default options merged with passed values.
    $options = array_merge([
      'allow_blocked' => true, // don't want to return a 404 for blocked users
      'allow_deleted' => false
    ], $options );

    $authenticate = User::getByUsernameAndVerifyPassword( $app, $username, $password, $options );

    $item = !empty( $authenticate['object'] ) ? $authenticate['object'] : null;


    // Check if user is in a blocked status.
    if( isset($authenticate['reason']) && $authenticate['reason'] == 'is_blocked' ) {

      // Log that the user is blocked.
      $app['monolog']->addWarning( $app->trans( 'logging.method-message-username-apikey-ip', [
        '%method%'  => 'AuthenticateController->authenticateUser()',
        '%message%'   => 'Authentication failed: User account blocked until `'.$item->login_blocked_until->format('Y-m-d H:i:s').'`',
        '%username%' => $username,
        '%api_key%'   => $obfuscated_api_key,
        '%ip_address%' => $app['remote_ip']
      ]));

      // Define the response.
      $validation_response = [
        'type' => 'authentication_failed',
        'code' => 'user_blocked',
        'message' => $app->trans( 'authorisation.user-account-blocked',
          ['%time%' => $item->login_blocked_until->format('h:i A')] )
      ];

      $item->dbUpdate();

      return $app->json( $validation_response, 422 );
    }


    // Check if simply not verified.
    if( !$authenticate || $authenticate['verified'] == false || empty( $item ) ) {

      $app['monolog']->addWarning( $app->trans( 'logging.method-message-username-apikey-ip', [
        '%method%'  => 'AuthenticateController->authenticateUser()',
        '%message%'   => 'Authentication failed: Username/password combination not found',
        '%username%' => $username,
        '%api_key%'   => $obfuscated_api_key,
        '%ip_address%' => $app['remote_ip']
      ]));

      // Define the response.
      $validation_response = [
        'type' => 'authentication_failed',
        'code' => 'invalid_credentials',
        'message' => $app->trans( 'authorisation.username-password-incorrect' )
      ];

      return $app->json( $validation_response, 422 );
    }


    /*
    * If here then user is authorised so can retrieve the User's embeds and return.
    */

    // Get embedded fields.
    foreach( array_keys( User::EMBED_FIELDS ) as $embed_fld ) {

      $embed_item = $item->getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

      if( !empty($embed_item) ) {
        $item->$embed_fld = $embed_item->toResponse();
      }
      else {
        $item->$embed_fld = sprintf( 'Not Found [ID:%s]', $item->$embed_fld );
      }
    }

    // Special embeds within the metadata.
    if( !empty( $item->metadata->country ) ) {

      $embed_item = Country::getByPkey( $app, $item->metadata->country );

      if( !empty($embed_item) ) {
        $item->metadata->country = $embed_item->toResponse();
      }
      else {
        $item->metadata->country = sprintf( 'Not Found [ID:%s]', $item->metadata->country );
      }
    }


    /*
    * Finally retrieve the user's permissions.
    */
    $permission_options = [
      'format' => 'objects',
      'model' => 'Medistudents\Models\AccountUser',
      'limit' => 'no_limit',
      'query_extra' => sprintf( "AND user_id = %d", $item->id )
    ];
    $permissions = $this->getMany( $app, $request, $permission_options );


    // If no result was found, return a standard 404.
    if( !empty($permissions) ) {

      foreach( $permissions as $k => $permission ) {

        $embed_fld = 'permission';
        $embed_item = $permission->getEmbedsByPKey( $app, $embed_fld, $permission->$embed_fld );

        if( !empty($embed_item) )
          $permission->$embed_fld = $embed_item->toResponse();
        else
          $permission->$embed_fld = 'Not Found';

        $permissions[$k] = $permission->toResponse(); // convert to correct schema for returning as API response.
      }

      $item->permissions = $permissions;
    }

    $user = $item->toResponse();
    $user['permissions'] = $permissions;

    return $app->json( $user, 200, $headers );
  } // public function authenticateUser( Application $app, Request $request )


  /**
  * Returns a User's permissions. If called using a non-master API key,
  * permissions will be limited to the calling API key's account only.
  */
  public function getUserPermissions( Application $app, Request $request, int $user_id ) {

    // Get the User's permissions.
    $options = [
      'format' => 'objects',
      'model' => 'Medistudents\Models\AccountUser',
      'limit' => 'no_limit',
      'query_extra' => sprintf( "AND user_id = %d", $user_id )
    ];
    $items = $this->getMany( $app, $request, $options );


    // If no result was found, return a standard 404.
    if( empty($items) ) {
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    // Embed the permission.
    foreach( $items as $k => $item ) {

      $embed_fld = 'permission';
      $embed_item = $item->getEmbedsByPKey( $app, $embed_fld, $item->$embed_fld );

      if( !empty($embed_item) )
        $item->$embed_fld = $embed_item->toResponse();
      else
        $item->$embed_fld = 'Not Found';

      $items[$k] = $item->toResponse(); // convert to correct schema for returning as API response.
    }

    return $app->json( $items, 200 );
  } // public function getUserPermissions( Application $app, int $user_id )


  /**
  * Initiates a forgotten password request.
  */
  public function forgotPassword( Application $app, Request $request ) {


    // Following used for logging.
    $provided_api_key = $_SERVER['PHP_AUTH_USER'];
    $obfuscated_api_key = substr( $provided_api_key, 0, 6 ) . '**************************';

    $headers = [];
    $options = [];

    $username = $request->get('username');

    $item = User::getByIdentifier( $app, $username, 'email' );


    // If no result was found, return a standard 404.
    if( empty( $item ) ) {

      $app['monolog']->addWarning( $app->trans( 'logging.method-message-username-apikey-ip', [
        '%method%'  => 'AuthenticateController->forgotPassword()',
        '%message%'   => 'Forgot password request failed: Username not found',
        '%username%' => $username,
        '%api_key%'   => $obfuscated_api_key,
        '%ip_address%' => $app['remote_ip']
      ]));

      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    // Create a new password reset hash and expiry date for it.
    $now = new \DateTime('now');

    $item->password_reset_hash = strtolower(Utilities::generateRandomString( 32 ));
    $item->password_reset_expiry = $now->add(new \DateInterval('PT'.$app['app.authorisation.password_reset_valid_for'].'S'));


    if( $item->dbUpdate() ) {

      $response = [
        'type' => 'success',
        'message' => $app->trans( 'authorisation.forgotten-password-request-made',
          ['%username%' => $username] ),
        'reset_code' => $item->password_reset_hash,
        'reset_expiry' => $item->password_reset_expiry->format('Y-m-d H:i:s')
      ];
      return $app->json( $response, 200 );
    }

    // At this point, non-update is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => 'update', '%name%' => User::NICE_NAME_SINGLE]), 500);


  } // public function forgotPassword( Application $app, Request $request )


  /**
  * Resets a password if a valid code is passed.
  */
  public function resetPassword( Application $app, Request $request, String $reset_code ) {

    // Following used for logging.
    $provided_api_key = $_SERVER['PHP_AUTH_USER'];
    $obfuscated_api_key = substr( $provided_api_key, 0, 6 ) . '**************************';

    $headers = [];
    $options = [];

    $now = new \DateTime('now');


    if( empty( $reset_code ) ) {

      $app['monolog']->addWarning( $app->trans( 'logging.method-message-apikey-ip', [
        '%method%'  => 'AuthenticateController->resetPassword()',
        '%message%'   => 'Reset password request failed: No code passed',
        '%api_key%'   => $obfuscated_api_key,
        '%ip_address%' => $app['remote_ip']
      ]));

      // Define the response.
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }


    // Get user by reset code
    $item = User::getByIdentifier( $app, $reset_code, 'password_reset_hash' );


    // If no result was found, return a standard 404.
    if( empty( $item ) ) {

      $app['monolog']->addWarning( $app->trans( 'logging.method-message-username-apikey-ip', [
        '%method%'  => 'AuthenticateController->resetPassword()',
        '%message%'   => 'Reset password request failed: Username not found by password reset code',
        '%username%' => 'Unknown (reset code was: '.$reset_code.')',
        '%api_key%'   => $obfuscated_api_key,
        '%ip_address%' => $app['remote_ip']
      ]));

      // Define the response.
      $http_code = 404;
      $error = HttpErrorResponses::getErr( $http_code );
      return $app->json( $error, $http_code );
    }

    else if( $reset_code !== $item->password_reset_hash || empty($item->password_reset_expiry) ) {

      $app['monolog']->addWarning( $app->trans( 'logging.method-message-username-apikey-ip', [
        '%method%'  => 'AuthenticateController->resetPassword()',
        '%message%'   => 'Reset password request failed: Reset code invalid',
        '%username%' => $item->email,
        '%api_key%'   => $obfuscated_api_key,
        '%ip_address%' => $app['remote_ip']
      ]));

      // Define the response.
      $validation_response = [
        'type' => 'reset_password_failed',
        'code' => 'invalid_code',
        'message' => $app->trans( 'authorisation.username-reset-code-invalid' )
      ];

      return $app->json( $validation_response, 422 );
    }

    else if( $now > $item->password_reset_expiry ) {

      $app['monolog']->addWarning( $app->trans( 'logging.method-message-username-apikey-ip', [
        '%method%'  => 'AuthenticateController->resetPassword()',
        '%message%'   => 'Reset password request failed: Reset code expired',
        '%username%' => $item->email,
        '%api_key%'   => $obfuscated_api_key,
        '%ip_address%' => $app['remote_ip']
      ]));

      // Define the response.
      $validation_response = [
        'type' => 'reset_password_failed',
        'code' => 'expired_code',
        'message' => $app->trans( 'authorisation.username-reset-code-expired' )
      ];

      return $app->json( $validation_response, 422 );
    }

    else if( empty($request->get('password')) ) {

      $validation_response = [
        'type' => 'reset_password_failed',
        'code' => 'password_missing',
        'message' => $app->trans( 'authorisation.password-required-to-update' )
      ];

      return $app->json( $validation_response, 422 );
    }

    else if( $item->email == $request->get('password') ) {

      $validation_response = [
        'type' => 'reset_password_failed',
        'code' => 'password_matches_username',
        'message' => $app->trans( 'authorisation.password-matches-username' )
      ];

      return $app->json( $validation_response, 422 );
    }


    $new_password_hash = Utilities::toPassword(
      $app['app.security.algo'],
      $request->get('password'),
      $item->email,
      $app['app.security.salt'],
      $app['app.security.cost'] );


    $item->password = $new_password_hash;
    $item->password_reset_hash = null;
    $item->password_reset_expiry = null;


    if( $item->dbUpdate() ) {
      return $app->json( $item->toResponse(), 200, $headers );
    }

    // At this point, non-update is a server error
    return new Response( $app->trans('Application failed to %action% %name%',
      ['%action%' => 'update', '%name%' => User::NICE_NAME_SINGLE]), 500);

  } // public function resetPassword( Application $app, Request $request )


} // class
