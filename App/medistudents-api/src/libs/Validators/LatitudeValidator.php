<?php
namespace Medistudents\Validators;

use Medistudents\Helpers\Utilties,
  Symfony\Component\Validator\Constraint,
  Symfony\Component\Validator\ConstraintValidator,
  Symfony\Component\Validator\Exception\UnexpectedTypeException;


class LatitudeValidator extends ConstraintValidator {

  const LATITUDE_MIN = -90;
  const LATITUDE_MAX = 90;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {

    if( !$constraint instanceof Latitude ) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Latitude');
    }

    if( null === $value ) {
      return;
    }

    if( !is_numeric($value) || stripos( strval($value), 'NaN' ) ) {
      throw new UnexpectedTypeException($value, 'latitude');
    }

    $valid = true;
    $value = \Medistudents\Helpers\Utilities::toFloat( $value );



    if( $value < self::LATITUDE_MIN || $value > self::LATITUDE_MAX ) {
      $valid = false;
    }


    if( !$valid ) {
      $this->context->buildViolation($constraint->message)
          ->setParameter('{{ value }}', $this->formatValue($value))
          ->setCode(Latitude::INVALID_LATITUDE_ERROR)
          ->addViolation();
    }
  }
}
