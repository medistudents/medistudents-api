<?php
namespace Medistudents\Validators;

use Medistudents\Helpers\Utilties,
  Medistudents\Models\ApiKey,
  Symfony\Component\Validator\Constraint,
  Symfony\Component\Validator\ConstraintValidator,
  Symfony\Component\Validator\Exception\UnexpectedTypeException;


class KeyTypeValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
// TODO: Why does this fail when updating a subscription that has no change to metadata?
    if( !$constraint instanceof KeyType ) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\KeyType');
    }

    if( null === $value ) {
      return;
    }

    $valid = false;

    if( in_array( $value, [
      // Do not include "MASTER" keys to be validated as they can not
      // be created by the application, only by manually editing the DB.
      ApiKey::API_KEYTYPE_STANDARD,
      ApiKey::API_KEYTYPE_RESTRICTED ] )
    ) {
      $valid = true;
    }


    if( !$valid ) {
      $this->context->buildViolation($constraint->message)
          ->setParameter('{{ value }}', $this->formatValue($value))
          ->setCode(KeyType::INVALID_KEYTYPE_ERROR)
          ->addViolation();
    }
  }
}
