<?php
namespace Medistudents\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class KeyValue extends Constraint {

  const INVALID_KEYVALUE_ERROR = '7b0b0d00-5def-48b8-a6eb-23b0bb1f543c';

  protected static $errorNames = array(
    self::INVALID_KEYVALUE_ERROR => 'INVALID_KEYVALUE_ERROR',
  );

  public $message = 'This value is not valid key-value data.';
}
