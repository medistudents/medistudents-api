<?php
namespace Medistudents\Validators;

use Medistudents\Helpers\Utilities as Utilities,
    Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator,
    Symfony\Component\Validator\Exception\UnexpectedTypeException;


class KeyValueValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {

    if( !$constraint instanceof KeyValue ) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\KeyValue');
    }

    if( null === $value ) {
      return;
    }


    $is_valid = true;

    if( is_string($value) ) { // convert to a PHP array if currently a JSON array
      $value = Utilities::isKeyValueJson( $value, true );
      $value = $value ? (array)$value : false;
    }
    if( !$value || !is_array($value) ) { // check if is an actual array
      $is_valid = false;
    }

    // Each key should be either an int or a string, and each value a
    // scalar value or null.
    else {

      if( 0 == count($value) ) {
        return;
      }

      foreach( $value as $k=>$v ) {

        if( !is_int($k) && !is_string($k) ) {
          $is_valid = false;
          break;
        }
      }
    }

    if( !$is_valid ) {
      $this->context->buildViolation($constraint->message)
          ->setParameter('{{ value }}', $this->formatValue($value))
          ->setCode(KeyValue::INVALID_KEYVALUE_ERROR)
          ->addViolation();
    }
  }
}
