<?php
namespace Medistudents\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Timezone extends Constraint {

  const INVALID_TIMEZONE_ERROR = '2e6d81aa-d24b-4b26-8fe2-da77cdca620f'; 

  protected static $errorNames = array(
    self::INVALID_TIMEZONE_ERROR => 'INVALID_TIMEZONE_ERROR',
  );

  public $message = 'This is not a valid timezone value.';
}
