<?php
namespace Medistudents\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Language extends Constraint {

  const INVALID_LANGUAGE_ERROR = '02aabe4c-2b56-49be-99d7-e976ee40fa1d'; 

  protected static $errorNames = array(
    self::INVALID_LANGUAGE_ERROR => 'INVALID_LANGUAGE_ERROR',
  );

  public $message = 'This is not a valid language value.';
}
