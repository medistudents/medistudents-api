<?php
namespace Medistudents\Validators;

use Medistudents\Helpers\Utilties,
  Medistudents\Models\Timezone as TimezoneModel,
  Symfony\Component\Validator\Constraint,
  Symfony\Component\Validator\ConstraintValidator,
  Symfony\Component\Validator\Exception\UnexpectedTypeException;


class TimezoneValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {

    if( !$constraint instanceof Timezone ) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Timezone');
    }

    if( null === $value ) {
      return;
    }

    global $app; // IMPROVE: Figure out how to not need a global here.
    $check_timezone = true;

    if( false !== filter_var( $value, FILTER_VALIDATE_INT, [
      'options' => [ 'default' => false ] ]))
    {
      $check_timezone = TimezoneModel::getByPKey( $app, [$value] );
    }
    else {
      $check_timezone = TimezoneModel::getByIdentifier( $app, [$value], 'timezone' );
    }


    if( !$check_timezone ) {
      $this->context->buildViolation($constraint->message)
          ->setParameter('{{ value }}', $this->formatValue($value))
          ->setCode(Timezone::INVALID_TIMEZONE_ERROR)
          ->addViolation();
    }
  }
}
