<?php
namespace Medistudents\Validators;

use Symfony\Component\Validator\Constraint,
  Symfony\Component\Validator\ConstraintValidator,
  Symfony\Component\Validator\Exception\UnexpectedTypeException;


class LongitudeValidator extends ConstraintValidator {

  const LONGITUDE_MIN = -180;
  const LONGITUDE_MAX = 180;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {

    if( !$constraint instanceof Longitude ) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Longitude');
    }

    if( null === $value ) {
      return;
    }

    if( !is_numeric($value) || stripos( strval($value), 'NaN' ) ) {
      throw new UnexpectedTypeException($value, 'number');
    }

    $valid = true;
    $value = \Medistudents\Helpers\Utilities::toFloat( $value );

    if( $value < self::LONGITUDE_MIN || $value > self::LONGITUDE_MAX ) {
      $valid = false;
    }


    if( !$valid ) {
      $this->context->buildViolation($constraint->message)
          ->setParameter('{{ value }}', $this->formatValue($value))
          ->setCode(Longitude::INVALID_LONGITUDE_ERROR)
          ->addViolation();
    }
  }
}
