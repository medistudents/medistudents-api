<?php
namespace Medistudents\Validators;

use Medistudents\Helpers\Utilties,
  Medistudents\Models\Language as LanguageModel,
  Symfony\Component\Validator\Constraint,
  Symfony\Component\Validator\ConstraintValidator,
  Symfony\Component\Validator\Exception\UnexpectedTypeException;


class LanguageValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {

    if( !$constraint instanceof Language ) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Language');
    }

    if( null === $value ) {
      return;
    }

    global $app; // IMPROVE: Figure out how to not need a global here.

    $check_language = LanguageModel::getByPKey( $app, [$value] );

    if( !$check_language ) {
      $this->context->buildViolation($constraint->message)
          ->setParameter('{{ value }}', $this->formatValue($value))
          ->setCode(Language::INVALID_LANGUAGE_ERROR)
          ->addViolation();
    }
  }
}
