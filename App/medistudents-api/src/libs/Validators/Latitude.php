<?php
namespace Medistudents\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Latitude extends Constraint {

  const INVALID_LATITUDE_ERROR = '6ce057b1-358e-4490-8f8f-4f054e1594ce';

  protected static $errorNames = array(
    self::INVALID_LATITUDE_ERROR => 'INVALID_LATITUDE_ERROR',
  );

  public $message = 'This is not a valid latitude value.';
}
