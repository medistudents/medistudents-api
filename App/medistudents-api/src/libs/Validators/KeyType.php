<?php
namespace Medistudents\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class KeyType extends Constraint {

  const INVALID_KEYTYPE_ERROR = '4b819057-55f3-47df-b77a-c705987473cb'; 

  protected static $errorNames = array(
    self::INVALID_KEYTYPE_ERROR => 'INVALID_KEYTYPE_ERROR',
  );

  public $message = 'This is not a valid `API key type` value.';
}
