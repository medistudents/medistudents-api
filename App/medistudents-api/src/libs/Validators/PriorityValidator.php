<?php
namespace Medistudents\Validators;

use Medistudents\Helpers\Utilties,
  Symfony\Component\Validator\Constraint,
  Symfony\Component\Validator\ConstraintValidator,
  Symfony\Component\Validator\Exception\UnexpectedTypeException;


class PriorityValidator extends ConstraintValidator {

  const PRIORITY_MIN = 0;
  const PRIORITY_MAX = 9;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {

    if( !$constraint instanceof Priority ) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Priority');
    }

    if( null === $value ) {
      return;
    }

    $valid = true;

    if( false === filter_var( $value, FILTER_VALIDATE_INT, [
      'options' => [
        'default' => false,
        'min_range' => self::PRIORITY_MIN,
        'max_range' => self::PRIORITY_MAX
      ]
    ])) {

      $valid = false;
    }


    if( !$valid ) {
      $this->context->buildViolation($constraint->message)
          ->setParameter('{{ value }}', $this->formatValue($value))
          ->setCode(Priority::INVALID_PRIORITY_ERROR)
          ->addViolation();
    }
  }
}
