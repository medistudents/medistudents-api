<?php
namespace Medistudents\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Priority extends Constraint {

  const INVALID_PRIORITY_ERROR = '8774bf22-8fda-4d40-9342-0fe45f022472'; 

  protected static $errorNames = array(
    self::INVALID_PRIORITY_ERROR => 'INVALID_PRIORITY_ERROR',
  );

  public $message = 'This is not a valid priority value.';
}
