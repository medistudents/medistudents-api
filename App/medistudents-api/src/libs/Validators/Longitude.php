<?php
namespace Medistudents\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Longitude extends Constraint {

  const INVALID_LONGITUDE_ERROR = '85c4eb11-e2e4-45f8-8cfe-bdff6f7abb0f';

  protected static $errorNames = array(
    self::INVALID_LONGITUDE_ERROR => 'INVALID_LONGITUDE_ERROR',
  );

  public $message = 'This is not a valid longitude value.';
}
